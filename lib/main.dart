import 'package:flutter/material.dart';
import 'package:kaykey/app.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const KaykeyApp());
}
