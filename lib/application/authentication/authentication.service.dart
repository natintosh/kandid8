import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:kaykey/domain/authentication/authentication.facade.dart';
import 'package:kaykey/domain/common/failure.dart';
import 'package:kaykey/domain/common/result.dart';
import 'package:kaykey/domain/common/text.value.dart';
import 'package:kaykey/domain/common/value.dart';

const String kaykeyDashboardEndpoint =
    'https://anycollar-51873.web.app/dashboard';

class AuthenticationService {
  final AuthenticationFacade? _repository;

  AuthenticationService({required AuthenticationFacade? repository})
      : _repository = repository;

  Future<Result<Failure<CreateUserErrorCode>, UserCredential>>
      createUserWithEmailAndPassword(
          {required TextValue displayName,
          required EmailAddressValue emailAddress,
          required PasswordValue password}) async {
    return _repository!.createUserWithEmailAndPassword(
        displayName: displayName,
        emailAddress: emailAddress,
        password: password);
  }

  Future<Result<Failure<SignInErrorCode>, UserCredential>>
      signInWithEmailAndPassword(
          {required EmailAddressValue emailAddress,
          required PasswordValue password}) {
    return _repository!.signInWithEmailAndPassword(
        emailAddress: emailAddress, password: password);
  }

  Future<Result<Failure<SignInErrorCode>, UserCredential>> signInWithGoogle() {
    return _repository!.signInWithGoogle();
  }

  Future<Result<Failure<EmailVerificationErrorCode>, Unit>>
      sendEmailVerificationCode() async {
    String url = kaykeyDashboardEndpoint;

    ActionCodeSettings? settings =
        ActionCodeSettings(url: url, handleCodeInApp: false);

    return _repository!.sendEmailVerification(settings: settings);
  }

  Future<Result<Unit, User?>> getFirebaseUser() {
    return _repository!.getFirebaseUser();
  }

  Future<Result<Unit, Unit>> isUserVerified() async {
    return _repository!.isUserVerified();
  }

  Future<Result<Failure<ActionCodeErrorCode>, ActionCodeInfo>> checkActionCode(
      {required TextValue actionCode}) async {
    return _repository!.checkActionCode(actionCode: actionCode);
  }

  Future<Result<Failure<ActionCodeErrorCode>, Unit>> applyActionCode(
      {required TextValue actionCode}) {
    return _repository!.applyActionCoode(actionCode: actionCode);
  }
}
