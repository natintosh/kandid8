import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:go_router/go_router.dart';
import 'package:kaykey/common/device/device.dart';
import 'package:kaykey/common/routers/app_router.dart';
import 'package:kaykey/common/themes/themes.dart';

class KaykeyApp extends StatelessWidget {
  const KaykeyApp({Key? key}) : super(key: key);

  static final GoRouter _router = AppRouter.router;
  static final GlobalKey<ScaffoldState> globalScaffoldKey =
      GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      theme: KaykeyTheme.lightTheme,
      themeMode: ThemeMode.light,
      routeInformationParser: _router.routeInformationParser,
      routerDelegate: _router.routerDelegate,
      supportedLocales: AppLocalizations.supportedLocales,
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      onGenerateTitle: (BuildContext context) {
        return AppLocalizations.of(context)!.title;
      },
      builder: (context, child) {
        return Scaffold(
          key: globalScaffoldKey,
          body: FutureBuilder<FirebaseApp>(
            future: Firebase.initializeApp(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return child!;
              } else if (snapshot.hasError) {
                return child!;
              }

              return child!;
            },
          ),
        );
      },
    );
  }
}
