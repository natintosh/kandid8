import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:kaykey/domain/authentication/authentication.facade.dart';
import 'package:kaykey/domain/common/failure.dart';
import 'package:kaykey/domain/common/result.dart';
import 'package:kaykey/domain/common/text.value.dart';
import 'package:kaykey/domain/common/value.dart';

class AuthenticationFacadeImpl implements AuthenticationFacade {
  final FirebaseAuth _firebaseAuth;

  const AuthenticationFacadeImpl({required FirebaseAuth firebaseAuth})
      : _firebaseAuth = firebaseAuth;

  @override
  Future<Result<Failure<CreateUserErrorCode>, UserCredential>>
      createUserWithEmailAndPassword(
          {required TextValue displayName,
          required EmailAddressValue emailAddress,
          required PasswordValue password}) async {
    String $displayName = displayName.value.getOrElse(() => '');
    String $emailAddress = emailAddress.value.getOrElse(() => '');
    String $password = password.value.getOrElse(() => '');

    try {
      UserCredential userCredentials =
          await _firebaseAuth.createUserWithEmailAndPassword(
              email: $emailAddress, password: $password);

      await userCredentials.user!.updateDisplayName($displayName);

      await userCredentials.user!.reload();

      return Result.onSuccess(value: userCredentials);
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case 'email-already-in-use':
          return Result.onFailure(
              value: Failure(
                  error: CreateUserErrorCode.emailAlreadyInUse,
                  message: e.message));
        case 'invalid-email':
          return Result.onFailure(
              value: Failure(
                  error: CreateUserErrorCode.invalidEmail, message: e.message));
        case 'weak-password':
          return Result.onFailure(
              value: Failure(
                  error: CreateUserErrorCode.weakPassword, message: e.message));
        default:
          return Result.onFailure(
            value: Failure(
                error: CreateUserErrorCode.internalError, message: e.message),
          );
      }
    }
  }

  @override
  Future<Result<Failure<SignInErrorCode>, UserCredential>>
      signInWithEmailAndPassword(
          {required EmailAddressValue emailAddress,
          required PasswordValue password}) async {
    try {
      String $emailAddress = emailAddress.value.getOrElse(() => '');
      String $password = password.value.getOrElse(() => '');

      UserCredential userCredential =
          await _firebaseAuth.signInWithEmailAndPassword(
              email: $emailAddress, password: $password);

      return Result.onSuccess(value: userCredential);
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case 'invalid-email':
          return Result.onFailure(
              value: Failure(
                  error: SignInErrorCode.invalidEmail, message: e.message));
        case 'user-disabled':
          return Result.onFailure(
              value: Failure(
                  error: SignInErrorCode.userDisabled, message: e.message));
        case 'user-not-found':
          return Result.onFailure(
              value: Failure(
                  error: SignInErrorCode.userNotFound, message: e.message));
        case 'wrong-password':
          return Result.onFailure(
              value: Failure(
                  error: SignInErrorCode.wrongPassword, message: e.message));
        default:
          return Result.onFailure(
              value: Failure(
                  error: SignInErrorCode.wrongPassword, message: e.message));
      }
    }
  }

  @override
  Future<Result<Failure<SignInErrorCode>, UserCredential>>
      signInWithGoogle() async {
    try {
      final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

      final GoogleSignInAuthentication? googleAuth =
          await googleUser?.authentication;

      final OAuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth?.accessToken,
        idToken: googleAuth?.idToken,
      );

      UserCredential userCredential =
          await _firebaseAuth.signInWithCredential(credential);

      return Result.onSuccess(value: userCredential);
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case 'invalid-email':
          return Result.onFailure(
              value: Failure(
                  error: SignInErrorCode.invalidEmail, message: e.message));
        case 'user-disabled':
          return Result.onFailure(
              value: Failure(
                  error: SignInErrorCode.userDisabled, message: e.message));
        case 'user-not-found':
          return Result.onFailure(
              value: Failure(
                  error: SignInErrorCode.userNotFound, message: e.message));
        case 'wrong-password':
          return Result.onFailure(
              value: Failure(
                  error: SignInErrorCode.wrongPassword, message: e.message));
        default:
          return Result.onFailure(
              value: Failure(
                  error: SignInErrorCode.wrongPassword, message: e.message));
      }
    }
  }

  // TODO: Handle other exceptions cases
  @override
  Future<Result<Failure<EmailVerificationErrorCode>, Unit>>
      sendEmailVerification({required ActionCodeSettings? settings}) async {
    try {
      await _firebaseAuth.currentUser!.sendEmailVerification(settings);

      return Result.onSuccess(value: unit);
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        default:
          return Result.onFailure(
            value: Failure(error: EmailVerificationErrorCode.internalError),
          );
      }
    }
  }

  @override
  Future<Result<Unit, Unit>> isUserVerified() async {
    try {
      await _firebaseAuth.currentUser!.reload();

      User? user = _firebaseAuth.currentUser;

      bool emailVerified = user!.emailVerified;

      return emailVerified
          ? Result.onSuccess(value: unit)
          : Result.onFailure(value: unit);
    } catch (e) {
      return Result.onFailure(value: unit);
    }
  }

  @override
  Future<Result<Unit, User?>> getFirebaseUser() async {
    try {
      User? user = _firebaseAuth.currentUser;

      user ??= await _firebaseAuth.userChanges().first;

      return _firebaseAuth.currentUser != null
          ? Result.onSuccess(value: _firebaseAuth.currentUser)
          : Result.onFailure(value: unit);
    } catch (e) {
      return Result.onFailure(value: unit);
    }
  }

  @override
  Future<Result<Failure<ActionCodeErrorCode>, ActionCodeInfo>> checkActionCode(
      {required TextValue actionCode}) async {
    try {
      String code = actionCode.value.getOrElse(() => '');
      ActionCodeInfo actionCodeInfo = await _firebaseAuth.checkActionCode(code);

      return Result.onSuccess(value: actionCodeInfo);
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case "expired-action-code":
          return Result.onFailure(
              value: Failure(
                  error: ActionCodeErrorCode.expiredActionCode,
                  message: e.message));
        case "invalid-action-code":
          return Result.onFailure(
              value: Failure(
                  error: ActionCodeErrorCode.invalidActionCode,
                  message: e.message));
        case "user-disabled":
          return Result.onFailure(
              value: Failure(
                  error: ActionCodeErrorCode.userDisabled, message: e.message));
        case "user-not-found":
          return Result.onFailure(
              value: Failure(
                  error: ActionCodeErrorCode.userNotFound, message: e.message));
        default:
          return Result.onFailure(
              value: Failure(
                  error: ActionCodeErrorCode.internal, message: e.message));
      }
    }
  }

  @override
  Future<Result<Failure<ActionCodeErrorCode>, Unit>> applyActionCoode(
      {required TextValue actionCode}) async {
    try {
      String code = actionCode.value.getOrElse(() => '');

      await _firebaseAuth.applyActionCode(code);

      return Result.onSuccess(value: unit);
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case "expired-action-code":
          return Result.onFailure(
              value: Failure(
                  error: ActionCodeErrorCode.expiredActionCode,
                  message: e.message));
        case "invalid-action-code":
          return Result.onFailure(
              value: Failure(
                  error: ActionCodeErrorCode.invalidActionCode,
                  message: e.message));
        case "user-disabled":
          return Result.onFailure(
              value: Failure(
                  error: ActionCodeErrorCode.userDisabled, message: e.message));
        case "user-not-found":
          return Result.onFailure(
              value: Failure(
                  error: ActionCodeErrorCode.userNotFound, message: e.message));
        default:
          return Result.onFailure(
              value: Failure(
                  error: ActionCodeErrorCode.internal, message: e.message));
      }
    }
  }
}
