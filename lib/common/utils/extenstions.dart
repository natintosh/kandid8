import 'package:flutter/material.dart';

extension MathsHelperX on double {
  double interpolate(
      {required double from,
      required double to,
      required double max,
      required double min}) {
    return from + ((this - min) * (to - from) / (max - min));
  }
}

extension TextThemeHelperX on TextStyle {
  TextStyle primary(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return copyWith(color: themeData.primaryColor);
  }
}
