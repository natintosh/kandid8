import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kaykey/common/themes/colors.dart';

class KaykeyTheme {
  static ThemeData get lightTheme => ThemeData.light().copyWith(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        pageTransitionsTheme: const PageTransitionsTheme(
          builders: <TargetPlatform, PageTransitionsBuilder>{
            TargetPlatform.android: ZoomPageTransitionsBuilder(),
            TargetPlatform.iOS: CupertinoPageTransitionsBuilder()
          },
        ),
        colorScheme: lightColorScheme,
        textTheme: textTheme(color: KaykeyColors.onBackground),
        primaryTextTheme: textTheme(color: KaykeyColors.onPrimary),
        textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
            padding: const EdgeInsets.all(25),
          ),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: TextButton.styleFrom(
            padding: const EdgeInsets.all(25),
          ),
        ),
        outlinedButtonTheme: OutlinedButtonThemeData(
          style: TextButton.styleFrom(
            padding: const EdgeInsets.all(25),
          ),
        ),
        inputDecorationTheme: InputDecorationTheme(
          border: OutlineInputBorder(
            borderSide: BorderSide(
              color: KaykeyColors.border,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: KaykeyColors.border,
            ),
          ),
          contentPadding: const EdgeInsets.symmetric(
            horizontal: 30,
            vertical: 25,
          ),
          filled: true,
          fillColor: const Color(0xFFF0F0FD),
        ),
      );

  static get lightColorScheme => const ColorScheme(
        primary: KaykeyColors.primary,
        primaryVariant: KaykeyColors.primaryVariant,
        secondary: KaykeyColors.secondary,
        secondaryVariant: KaykeyColors.secondaryVariant,
        surface: KaykeyColors.surface,
        background: KaykeyColors.background,
        error: KaykeyColors.error,
        onPrimary: KaykeyColors.onPrimary,
        onSecondary: KaykeyColors.onSecondary,
        onSurface: KaykeyColors.onSurface,
        onBackground: KaykeyColors.onBackground,
        onError: KaykeyColors.onError,
        brightness: Brightness.light,
      );

  static TextTheme textTheme({required Color color}) {
    return TextTheme(
      headline1: GoogleFonts.lato(
        color: color,
        fontSize: 101,
        fontWeight: FontWeight.w300,
        letterSpacing: -1.5,
      ),
      headline2: GoogleFonts.lato(
        color: color,
        fontSize: 63,
        fontWeight: FontWeight.w300,
        letterSpacing: -0.5,
      ),
      headline3: GoogleFonts.lato(
        color: color,
        fontSize: 50,
        fontWeight: FontWeight.w400,
      ),
      headline4: GoogleFonts.lato(
        color: color,
        fontSize: 36,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.25,
      ),
      headline5: GoogleFonts.lato(
        color: color,
        fontSize: 25,
        fontWeight: FontWeight.w400,
      ),
      headline6: GoogleFonts.lato(
        color: color,
        fontSize: 21,
        fontWeight: FontWeight.w500,
        letterSpacing: 0.15,
      ),
      subtitle1: GoogleFonts.lato(
        color: color,
        fontSize: 17,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.15,
      ),
      subtitle2: GoogleFonts.lato(
        color: color,
        fontSize: 15,
        fontWeight: FontWeight.w500,
        letterSpacing: 0.1,
      ),
      bodyText1: GoogleFonts.lato(
        color: color,
        fontSize: 17,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.5,
      ),
      bodyText2: GoogleFonts.lato(
        color: color,
        fontSize: 15,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.25,
      ),
      button: GoogleFonts.lato(
        color: color,
        fontSize: 15,
        fontWeight: FontWeight.w500,
        letterSpacing: 1.25,
      ),
      caption: GoogleFonts.lato(
        color: color,
        fontSize: 13,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.4,
      ),
      overline: GoogleFonts.lato(
        color: color,
        fontSize: 10,
        fontWeight: FontWeight.w400,
        letterSpacing: 1.5,
      ),
    );
  }
}

extension KaykeyTextThemes on TextTheme {
  TextTheme get onTitle => KaykeyTheme.textTheme(color: KaykeyColors.secondary);

  TextTheme get onHyperText =>
      KaykeyTheme.textTheme(color: KaykeyColors.primary);
}
