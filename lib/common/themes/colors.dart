import 'package:flutter/material.dart';

class KaykeyColors {
  KaykeyColors._();

  static const double _borderOpacity = 0.45;

  static const Color primary = Color(0xFF7770F3);

  static const Color primaryVariant = Color(0xFF5952C6);

  static const Color secondary = Color(0xFFF9BE11);

  static const Color secondaryVariant = Color(0xFFFFDF97);

  static const Color surface = Color(0xFFFEFBFF);

  static const Color background = Color(0xFFFEFBFF);

  static const Color error = Color(0xFFBA1B1B);

  static const Color onPrimary = Color(0xFFFFFFFF);

  static const Color onSecondary = Color(0xFF3F2E00);

  static const Color onSurface = Color(0xFF030546);

  static const Color onBackground = Color(0xFF030546);

  static const Color onTitle = Color(0xFF1B1B1F);

  static const Color onError = Color(0xFFFFFFFF);

  static Color border = const Color(0xFF7770F3).withOpacity(_borderOpacity);

  static const Color inputFill = Color(0xFFF0F0FD);
}
