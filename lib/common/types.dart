import 'package:kaykey/domain/common/result.dart';

enum ProcessingState { loading, completed }

typedef ResultCallback<S, T> = void Function(Result<S, T> result);

typedef DataCallback<T> = T Function();

typedef ValueDataCallback<S, T> = S Function(T value);
