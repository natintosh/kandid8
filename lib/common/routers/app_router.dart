import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:kaykey/presentation/pages/authentication/email_verification/email_verification_page.dart';
import 'package:kaykey/presentation/pages/authentication/signin/signin_page.dart';
import 'package:kaykey/presentation/pages/authentication/signup/signup_page.dart';
import 'package:kaykey/presentation/pages/authentication/verify_email/verify_email_page.dart';
import 'package:kaykey/presentation/pages/error/error_page.dart';
import 'package:kaykey/presentation/pages/home_page.dart';
import 'package:kaykey/presentation/pages/index/index_page.dart';

class AppRouter {
  static GoRouter get router => GoRouter(
        urlPathStrategy: UrlPathStrategy.path,
        debugLogDiagnostics: true,
        routes: <GoRoute>[
          GoRoute(
            name: 'home',
            path: '/',
            routes: <GoRoute>[
              _signinRoutes,
              _signupRoutes,
              _emailVerification,
              _verifyEmail,
              _index,
            ],
            pageBuilder: (BuildContext context, GoRouterState state) {
              return MaterialPage(
                key: state.pageKey,
                child: const HomePage(),
              );
            },
          )
        ],
        errorPageBuilder: _errorPageBuilder,
      );

  static Page _errorPageBuilder(BuildContext context, GoRouterState state) {
    return MaterialPage(
      key: state.pageKey,
      child: const ErrorPage(),
    );
  }

  static GoRoute get _signinRoutes => GoRoute(
      name: 'signin',
      path: 'signin',
      // redirect: (GoRouterState state) {
      //   return '/index';
      // },
      pageBuilder: (BuildContext context, GoRouterState state) {
        return MaterialPage(
          key: state.pageKey,
          child: const SigninPage(),
        );
      });

  static GoRoute get _signupRoutes => GoRoute(
      name: 'signup',
      path: 'signup',
      pageBuilder: (BuildContext context, GoRouterState state) {
        return MaterialPage(
          key: state.pageKey,
          child: const SignUpPage(),
        );
      });

  static GoRoute get _emailVerification => GoRoute(
      name: 'emailVerification',
      path: 'emailVerification',
      pageBuilder: (BuildContext context, GoRouterState state) {
        return MaterialPage(
          key: state.pageKey,
          child: const EmailVerificationPage(),
        );
      });

  static GoRoute get _verifyEmail => GoRoute(
      name: "verifyEmail",
      path: "verifyEmail",
      pageBuilder: (BuildContext context, GoRouterState state) {
        String oobCode = state.queryParams['oobCode'] ?? '';
        return MaterialPage(
          key: state.pageKey,
          child: VerifyEmailPage(
            oobCode: oobCode,
          ),
        );
      });

  static GoRoute get _index => GoRoute(
      name: 'index',
      path: 'index',
      pageBuilder: (BuildContext context, GoRouterState state) {
        return MaterialPage(
          key: state.pageKey,
          child: const IndexPage(),
        );
      });
}
