import 'package:flutter/material.dart';
import 'package:kaykey/common/types.dart';
import 'package:kaykey/l10n/localization.dart';
import 'package:kaykey/presentation/notifiers/email_verification_notifier.dart';
import 'package:sized_context/sized_context.dart';
import 'package:provider/provider.dart';

class EmailVerificationButtons extends StatelessWidget {
  final VoidCallback onRequestNewLinkButtonPressed;
  final VoidCallback onAlreadyVerifiedButtonPressed;
  const EmailVerificationButtons(
      {Key? key,
      required this.onRequestNewLinkButtonPressed,
      required this.onAlreadyVerifiedButtonPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isProcessing =
        context.watch<EmailVerificationNotifier>().processingState ==
            ProcessingState.loading;
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: SizedBox(
            width: context.widthPct(0.6),
            child: TextButton(
                onPressed: isProcessing ? null : onRequestNewLinkButtonPressed,
                child: Text(AppLocalizations.of(context)!.requestNewLink)),
          ),
        ),
        const SizedBox(height: 16),
        Center(
          child: SizedBox(
            width: context.widthPct(0.6),
            child: ElevatedButton(
                onPressed: isProcessing ? null : onAlreadyVerifiedButtonPressed,
                child: Text(AppLocalizations.of(context)!.alreadyVerified)),
          ),
        ),
      ],
    );
  }
}
