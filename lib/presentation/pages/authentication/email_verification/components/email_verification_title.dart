import 'package:flutter/material.dart';
import 'package:kaykey/common/themes/themes.dart';
import 'package:kaykey/l10n/localization.dart';
import 'package:kaykey/presentation/widgets/responsive.dart';

class EmailVerificationTitle extends StatelessWidget {
  final String emailAddress;

  const EmailVerificationTitle({Key? key, required this.emailAddress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 64),
          child: Text(
            AppLocalizations.of(context)!.title,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.onTitle.headline5!.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 12),
          child: Text(
            AppLocalizations.of(context)!.verifyYourEmail,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 48),
          child: Text(
            AppLocalizations.of(context)!.needToVerify(emailAddress),
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 48),
          child: Text(
            AppLocalizations.of(context)!.didNotReceiveEmail(
                Responsive.isDesktop(context)
                    ? AppLocalizations.of(context)!.click
                    : AppLocalizations.of(context)!.tap),
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ),
      ],
    );
  }
}
