import 'package:flutter/material.dart';
import 'package:kaykey/l10n/localization.dart';
import 'package:kaykey/presentation/widgets/email_animation.dart';
import 'package:sized_context/sized_context.dart';

class EmailVerificationNotSent extends StatelessWidget {
  final VoidCallback onGoBackButtonPressed;

  const EmailVerificationNotSent(
      {Key? key, required this.onGoBackButtonPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double size = context.widthPx < context.heightPx
        ? context.widthPct(0.25)
        : context.heightPct(0.25);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        EmailAnimation(size: size),
        const SizedBox(height: 20),
        Text(AppLocalizations.of(context)!.accountNotFound),
        const SizedBox(height: 20),
        TextButton.icon(
            onPressed: onGoBackButtonPressed,
            icon: const Icon(Icons.arrow_back),
            label: Text(AppLocalizations.of(context)!.goBack))
      ],
    );
  }
}
