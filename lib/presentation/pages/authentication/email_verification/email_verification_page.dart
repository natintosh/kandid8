import 'package:dartz/dartz.dart' as dz;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:kaykey/application/authentication/authentication.service.dart';
import 'package:kaykey/domain/common/result.dart';
import 'package:kaykey/infrastructure/authentication/authentication.facade.dart';
import 'package:kaykey/presentation/notifiers/email_verification_notifier.dart';
import 'package:kaykey/presentation/pages/authentication/email_verification/components/email_verification_buttons.dart';
import 'package:kaykey/presentation/pages/authentication/email_verification/components/email_verification_not_sent.dart';
import 'package:kaykey/presentation/pages/authentication/email_verification/components/email_verification_title.dart';
import 'package:kaykey/presentation/widgets/loading_animation.dart';
import 'package:kaykey/presentation/widgets/responsive.dart';
import 'package:kaykey/presentation/widgets/widget_view.dart';
import 'package:provider/provider.dart';
import 'package:sized_context/sized_context.dart';

class EmailVerificationPage extends StatelessWidget {
  const EmailVerificationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) {
        return EmailVerificationNotifier(
            context,
            AuthenticationService(
                repository: AuthenticationFacadeImpl(
                    firebaseAuth: FirebaseAuth.instance)));
      },
      child: const _EmailVerificationPage(),
    );
  }
}

class _EmailVerificationPage extends StatefulWidget {
  const _EmailVerificationPage({Key? key}) : super(key: key);

  @override
  _EmailVerificationPageState createState() => _EmailVerificationPageState();
}

class _EmailVerificationPageState extends State<_EmailVerificationPage> {
  VoidCallback get onRequestNewLinkButtonPressed => () {
        sendEmailVerification();
      };

  VoidCallback get onAlreadyVerifiedButtonPressed => () {
        context.read<EmailVerificationNotifier>().checkUserVerificationStatus(
            onResult: (Result<dz.Unit, dz.Unit> result) {
          result.when(
              onSuccess: (_) {
                GoRouter.of(context).goNamed('index');
              },
              onFailure: (_) => {});
        });
      };

  get onGoBackButtonPressed => () {
        GoRouter.of(context).goNamed('home');
      };

  void sendEmailVerification() async {
    context.read<EmailVerificationNotifier>().sendVerificationEmail(
        onResult: (Result<dz.Unit, dz.Unit> result) {
      result.maybeWhen(onSuccess: (_) {}, orElse: () => Null);
    });
  }

  void retrieveEmailAddressAndSendEmailVerification() {
    context.read<EmailVerificationNotifier>().retrieveEmailAddress(
        onResult: (Result<dz.Unit, String?> result) {
      result.maybeWhen(
          onSuccess: (String? emailAddress) {
            context.read<EmailVerificationNotifier>().emailAddress =
                emailAddress;

            sendEmailVerification();
          },
          orElse: () => '');
    });
  }

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      retrieveEmailAddressAndSendEmailVerification();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double size = context.widthPx < context.heightPx
        ? context.widthPct(0.25)
        : context.heightPct(0.25);
    return context.watch<EmailVerificationNotifier>().isRetrievingEmailAddress
        ? Center(
            child: SizedBox.square(
              dimension: size,
              child: LoadingAnimation(
                size: size,
              ),
            ),
          )
        : Responsive(
            mobile: _MobileEmailVerificationView(this),
            tablet: _TabletEmailVerificationView(this),
            desktop: _DesktopEmailVerificationView(this),
          );
  }
}

class _MobileEmailVerificationView
    extends WidgetView<_EmailVerificationPage, _EmailVerificationPageState> {
  const _MobileEmailVerificationView(_EmailVerificationPageState state)
      : super(state);

  @override
  Widget build(BuildContext context) {
    return !context.watch<EmailVerificationNotifier>().canSendVerificationEmail
        ? EmailVerificationNotSent(
            onGoBackButtonPressed: state.onGoBackButtonPressed,
          )
        : ListView(
            padding: const EdgeInsets.only(left: 24, right: 24),
            children: [
              EmailVerificationTitle(
                  emailAddress:
                      context.watch<EmailVerificationNotifier>().emailAddress!),
              const SizedBox(height: 60),
              EmailVerificationButtons(
                onRequestNewLinkButtonPressed:
                    state.onRequestNewLinkButtonPressed,
                onAlreadyVerifiedButtonPressed:
                    state.onAlreadyVerifiedButtonPressed,
              ),
            ],
          );
  }
}

class _TabletEmailVerificationView
    extends WidgetView<_EmailVerificationPage, _EmailVerificationPageState> {
  const _TabletEmailVerificationView(_EmailVerificationPageState state)
      : super(state);

  @override
  Widget build(BuildContext context) {
    return !context.watch<EmailVerificationNotifier>().canSendVerificationEmail
        ? EmailVerificationNotSent(
            onGoBackButtonPressed: state.onGoBackButtonPressed,
          )
        : ListView(
            padding: const EdgeInsets.only(left: 100, right: 100),
            children: [
              EmailVerificationTitle(
                  emailAddress:
                      context.watch<EmailVerificationNotifier>().emailAddress!),
              const SizedBox(height: 60),
              EmailVerificationButtons(
                onRequestNewLinkButtonPressed:
                    state.onRequestNewLinkButtonPressed,
                onAlreadyVerifiedButtonPressed:
                    state.onAlreadyVerifiedButtonPressed,
              ),
            ],
          );
  }
}

class _DesktopEmailVerificationView
    extends WidgetView<_EmailVerificationPage, _EmailVerificationPageState> {
  const _DesktopEmailVerificationView(_EmailVerificationPageState state)
      : super(state);

  @override
  Widget build(BuildContext context) {
    return !context.watch<EmailVerificationNotifier>().canSendVerificationEmail
        ? EmailVerificationNotSent(
            onGoBackButtonPressed: state.onGoBackButtonPressed,
          )
        : ListView(
            padding: const EdgeInsets.only(left: 200, right: 200),
            children: [
              EmailVerificationTitle(
                  emailAddress:
                      context.watch<EmailVerificationNotifier>().emailAddress!),
              const SizedBox(height: 60),
              EmailVerificationButtons(
                onRequestNewLinkButtonPressed:
                    state.onRequestNewLinkButtonPressed,
                onAlreadyVerifiedButtonPressed:
                    state.onAlreadyVerifiedButtonPressed,
              ),
            ],
          );
  }
}
