import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:kaykey/common/themes/themes.dart';
import 'package:kaykey/l10n/localization.dart';

class SignupText extends StatelessWidget {
  final GestureRecognizer signupTextGestureRecognizer;

  const SignupText({
    Key? key,
    required this.signupTextGestureRecognizer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 60),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: AppLocalizations.of(context)!.dontHaveAnAccount,
          children: [
            TextSpan(
              text: AppLocalizations.of(context)!.signup,
              style: Theme.of(context).textTheme.onHyperText.bodyText2,
              recognizer: signupTextGestureRecognizer,
            )
          ],
          style: Theme.of(context).textTheme.bodyText2,
        ),
      ),
    );
  }
}
