import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:kaykey/common/themes/themes.dart';
import 'package:kaykey/l10n/localization.dart';
import 'package:kaykey/presentation/notifiers/signin_notifier.dart';
import 'package:provider/provider.dart';

class SigninForm extends StatelessWidget {
  final TextEditingController emailAddressController;
  final TextEditingController passwordController;

  final VoidCallback? onSigninButtonPressed;
  final GestureRecognizer onForgotPasswordGestureRecognizer;

  const SigninForm(
      {Key? key,
      required this.emailAddressController,
      required this.passwordController,
      required this.onSigninButtonPressed,
      required this.onForgotPasswordGestureRecognizer})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      primary: false,
      shrinkWrap: true,
      children: [
        ListTile(
          contentPadding: EdgeInsets.zero,
          title: Padding(
            padding: const EdgeInsets.only(bottom: 16),
            child: Text(
              AppLocalizations.of(context)!.emailAddress,
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
          subtitle: TextFormField(
            controller: emailAddressController,
            validator: (String? value) => context
                .read<SignInNotifier>()
                .emailAddressValidator(value,
                    onData: () =>
                        AppLocalizations.of(context)!.invalidEmailAddress),
            decoration: InputDecoration(
              hintText: AppLocalizations.of(context)!.emailPlaceholder,
            ),
          ),
        ),
        const SizedBox(height: 20),
        ListTile(
          contentPadding: EdgeInsets.zero,
          title: Padding(
            padding: const EdgeInsets.only(bottom: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppLocalizations.of(context)!.password,
                  style: Theme.of(context).textTheme.bodyText2,
                ),
                RichText(
                  text: TextSpan(
                    text: AppLocalizations.of(context)!.forgotPasssword,
                    style: Theme.of(context).textTheme.onHyperText.bodyText2,
                    recognizer: onForgotPasswordGestureRecognizer,
                  ),
                ),
              ],
            ),
          ),
          subtitle: TextFormField(
            controller: passwordController,
            obscureText: context.watch<SignInNotifier>().obscurePassword,
            validator: (String? value) => context
                .read<SignInNotifier>()
                .passwordValidator(value,
                    onData: () => AppLocalizations.of(context)!.passwordEmpty),
            decoration: InputDecoration(
                hintText: AppLocalizations.of(context)!.passwordPlacehold,
                suffixIcon: IconButton(
                    onPressed: context
                        .read<SignInNotifier>()
                        .onPasswordIconButtonPressed,
                    icon: Icon(context.watch<SignInNotifier>().obscurePassword
                        ? Icons.visibility
                        : Icons.visibility_off))),
          ),
        ),
        const SizedBox(height: 20),
        SizedBox(
          width: double.infinity,
          child: ElevatedButton(
            onPressed: onSigninButtonPressed,
            child: Text(
              AppLocalizations.of(context)!.signin,
            ),
          ),
        ),
      ],
    );
  }
}
