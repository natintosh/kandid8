import 'package:auth_buttons/auth_buttons.dart';
import 'package:flutter/material.dart';

class SigninWithButtons extends StatelessWidget {
  final VoidCallback onGoogleAuthButtonPressed;
  final VoidCallback onFaceBookAuthButtonPressed;

  const SigninWithButtons({
    Key? key,
    required this.onGoogleAuthButtonPressed,
    required this.onFaceBookAuthButtonPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView(
        primary: false,
        shrinkWrap: true,
        children: [
          GoogleAuthButton(
            onPressed: onGoogleAuthButtonPressed,
            style: AuthButtonStyle(
              iconSize: 24,
              width: double.infinity,
              borderRadius: 0,
              padding: const EdgeInsets.only(top: 16, bottom: 16),
              textStyle: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(fontWeight: FontWeight.bold),
            ),
          ),
          const SizedBox(height: 24),
          FacebookAuthButton(
            onPressed: onFaceBookAuthButtonPressed,
            style: AuthButtonStyle(
              iconSize: 24,
              width: double.infinity,
              borderRadius: 0,
              padding: const EdgeInsets.only(top: 16, bottom: 16),
              textStyle: Theme.of(context)
                  .primaryTextTheme
                  .bodyText1!
                  .copyWith(fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}
