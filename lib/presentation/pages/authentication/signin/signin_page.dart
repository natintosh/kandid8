import 'package:dartz/dartz.dart' as dz;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:kaykey/application/authentication/authentication.service.dart';
import 'package:kaykey/common/device/device.dart';
import 'package:kaykey/common/themes/colors.dart';
import 'package:kaykey/domain/authentication/authentication.facade.dart';
import 'package:kaykey/domain/common/result.dart';
import 'package:kaykey/infrastructure/authentication/authentication.facade.dart';
import 'package:kaykey/l10n/localization.dart';
import 'package:kaykey/presentation/notifiers/signin_notifier.dart';
import 'package:kaykey/presentation/pages/authentication/signin/components/signin_form.dart';
import 'package:kaykey/presentation/pages/authentication/signin/components/signin_title.dart';
import 'package:kaykey/presentation/pages/authentication/signin/components/signin_with_buttons.dart';
import 'package:kaykey/presentation/pages/authentication/signin/components/signup_text.dart';
import 'package:kaykey/presentation/widgets/display_error.dart';
import 'package:kaykey/presentation/widgets/responsive.dart';
import 'package:kaykey/presentation/widgets/widget_view.dart';
import 'package:provider/provider.dart';

class SigninPage extends StatelessWidget {
  const SigninPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!Device.isWindows) {
      return ChangeNotifierProvider(
        create: (context) {
          return SignInNotifier(AuthenticationService(
              repository: AuthenticationFacadeImpl(
                  firebaseAuth: FirebaseAuth.instance)));
        },
        child: const _SigninPage(),
      );
    }

    return ChangeNotifierProvider(
        create: (context) {
          return SignInNotifier(AuthenticationService(repository: null));
        },
        child: const _SigninPage());
  }
}

class _SigninPage extends StatefulWidget {
  const _SigninPage({Key? key}) : super(key: key);

  @override
  _SigninPageState createState() => _SigninPageState();
}

class _SigninPageState extends State<_SigninPage> {
  late final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  late final TextEditingController emailAddressController;
  late final TextEditingController passwordController;

  // TODO: Add google authentication
  get onGoogleAuthButtonPressed => () {
        context.read<SignInNotifier>().signInWithGoogle(
            onResult: (Result<dz.Unit, dz.Unit> result) {
          result.maybeWhen(
              onSuccess: (dz.Unit value) {
                goToDashboard();
              },
              orElse: () {});
        }, onData: (SignInErrorCode value) {
          switch (value) {
            default:
              return AppLocalizations.of(context)!.invalidEmailOrPassword;
          }
        });
      };

  // TODO: Add facebook authentication
  get onFaceBookAuthButtonPressed => () {};

  // TODO: Replace method with normal sign in login for deployment
  get onSigninButtonPressed => () {
        // goToDashboard();
        // return;
        // TODO: return dead code
        // ignore: dead_code
        if (formKey.currentState!.validate()) {
          context.read<SignInNotifier>().signInWithEmailAndPassword(
              onResult: (Result<dz.Unit, dz.Unit> result) {
            result.maybeWhen(
              onSuccess: (dz.Unit value) {
                goToDashboard();
              },
              orElse: () {},
            );
          }, onData: (SignInErrorCode value) {
            switch (value) {
              default:
                return AppLocalizations.of(context)!.invalidEmailOrPassword;
            }
          });
        }
      };

  GestureTapCallback? get onSignupButtonPressed => () {
        GoRouter.of(context).goNamed('signup');
      };

  GestureTapCallback? get onForgotPasswordButtonPressed => () {
        GoRouter.of(context).goNamed('forgotPassword');
      };

  VoidCallback get goToDashboard => () {
        GoRouter.of(context).goNamed('index');
      };

  get signupTextGestureRecognizer =>
      TapGestureRecognizer()..onTap = onSignupButtonPressed;

  get onForgotPasswordGestureRecognizer =>
      TapGestureRecognizer()..onTap = onForgotPasswordButtonPressed;

  void emailAddressListener() {
    context.read<SignInNotifier>().emailAddress = emailAddressController.text;
  }

  void passwordListener() {
    context.read<SignInNotifier>().password = passwordController.text;
  }

  @override
  void initState() {
    emailAddressController = TextEditingController()
      ..addListener(emailAddressListener);
    passwordController = TextEditingController()..addListener(passwordListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: Responsive(
          mobile: _MobileSigninPage(this),
          tablet: _TabletSigninPage(this),
          desktop: _DesktopSigninPageView(this),
        ),
      ),
    );
  }
}

class _MobileSigninPage extends WidgetView<_SigninPage, _SigninPageState> {
  const _MobileSigninPage(_SigninPageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SigninTitle(),
            const SizedBox(height: 60),
            SigninForm(
              emailAddressController: state.emailAddressController,
              passwordController: state.passwordController,
              onSigninButtonPressed:
                  context.watch<SignInNotifier>().isProcessing
                      ? null
                      : state.onSigninButtonPressed,
              onForgotPasswordGestureRecognizer:
                  state.onForgotPasswordGestureRecognizer,
            ),
            context.watch<SignInNotifier>().showError
                ? DisplayError(
                    message: context.watch<SignInNotifier>().errorMessage,
                    onCancelButtonPressed:
                        context.watch<SignInNotifier>().hideError,
                  )
                : const SizedBox.shrink(),
            const SizedBox(height: 24),
            const Divider(indent: 12, endIndent: 12),
            const SizedBox(height: 24),
            SigninWithButtons(
              onGoogleAuthButtonPressed: state.onGoogleAuthButtonPressed,
              onFaceBookAuthButtonPressed: state.onFaceBookAuthButtonPressed,
            ),
            SignupText(
              signupTextGestureRecognizer: state.signupTextGestureRecognizer,
            ),
          ],
        ),
      ),
    );
  }
}

class _TabletSigninPage extends WidgetView<_SigninPage, _SigninPageState> {
  const _TabletSigninPage(_SigninPageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SigninTitle(),
          Padding(
            padding: const EdgeInsets.only(top: 60)
                .add(const EdgeInsets.symmetric(horizontal: 48)),
            child: Row(
              children: [
                Expanded(
                  flex: 5,
                  child: SigninWithButtons(
                    onGoogleAuthButtonPressed: state.onGoogleAuthButtonPressed,
                    onFaceBookAuthButtonPressed:
                        state.onFaceBookAuthButtonPressed,
                  ),
                ),
                const SizedBox(width: 16),
                SizedBox(
                  height: 309,
                  child: VerticalDivider(
                    color: KaykeyColors.border,
                    indent: 12,
                    endIndent: 12,
                  ),
                ),
                const SizedBox(width: 16),
                Expanded(
                  flex: 7,
                  child: Column(
                    children: [
                      SigninForm(
                        emailAddressController: state.emailAddressController,
                        passwordController: state.passwordController,
                        onSigninButtonPressed:
                            context.watch<SignInNotifier>().isProcessing
                                ? null
                                : state.onSigninButtonPressed,
                        onForgotPasswordGestureRecognizer:
                            state.onForgotPasswordGestureRecognizer,
                      ),
                      context.watch<SignInNotifier>().showError
                          ? DisplayError(
                              message:
                                  context.watch<SignInNotifier>().errorMessage,
                              onCancelButtonPressed:
                                  context.watch<SignInNotifier>().hideError,
                            )
                          : const SizedBox.shrink(),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SignupText(
            signupTextGestureRecognizer: state.signupTextGestureRecognizer,
          ),
          const SizedBox(height: 65),
        ],
      ),
    );
  }
}

class _DesktopSigninPageView extends WidgetView<_SigninPage, _SigninPageState> {
  const _DesktopSigninPageView(_SigninPageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SigninTitle(),
          Padding(
            padding: const EdgeInsets.only(top: 60)
                .add(const EdgeInsets.symmetric(horizontal: 100)),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: SigninWithButtons(
                    onGoogleAuthButtonPressed: state.onGoogleAuthButtonPressed,
                    onFaceBookAuthButtonPressed:
                        state.onFaceBookAuthButtonPressed,
                  ),
                ),
                const SizedBox(width: 120),
                SizedBox(
                  height: 309,
                  child: VerticalDivider(
                    color: KaykeyColors.border,
                    indent: 12,
                    endIndent: 12,
                  ),
                ),
                const SizedBox(width: 120),
                Expanded(
                  flex: 4,
                  child: Column(
                    children: [
                      SigninForm(
                        emailAddressController: state.emailAddressController,
                        passwordController: state.passwordController,
                        onSigninButtonPressed:
                            context.watch<SignInNotifier>().isProcessing
                                ? null
                                : state.onSigninButtonPressed,
                        onForgotPasswordGestureRecognizer:
                            state.onForgotPasswordGestureRecognizer,
                      ),
                      context.watch<SignInNotifier>().showError
                          ? DisplayError(
                              message:
                                  context.watch<SignInNotifier>().errorMessage,
                              onCancelButtonPressed:
                                  context.watch<SignInNotifier>().hideError,
                            )
                          : const SizedBox.shrink(),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SignupText(
            signupTextGestureRecognizer: state.signupTextGestureRecognizer,
          ),
          const SizedBox(height: 65),
        ],
      ),
    );
  }
}
