import 'package:dartz/dartz.dart' as dz;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:kaykey/application/authentication/authentication.service.dart';
import 'package:kaykey/domain/common/result.dart';
import 'package:kaykey/domain/common/text.value.dart';
import 'package:kaykey/infrastructure/authentication/authentication.facade.dart';
import 'package:kaykey/l10n/localization.dart';
import 'package:kaykey/presentation/notifiers/verify_email_notifier.dart';
import 'package:kaykey/presentation/widgets/loading_animation.dart';
import 'package:kaykey/presentation/widgets/responsive.dart';
import 'package:kaykey/presentation/widgets/widget_view.dart';
import 'package:provider/provider.dart';
import 'package:sized_context/sized_context.dart';

class VerifyEmailPage extends StatelessWidget {
  final String oobCode;

  const VerifyEmailPage({Key? key, required this.oobCode}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) {
        return VerifyEmailNotifier(
            context,
            AuthenticationService(
                repository: AuthenticationFacadeImpl(
                    firebaseAuth: FirebaseAuth.instance)))
          ..actionCode = TextValue(oobCode)
          ..checkForUserInstance();
      },
      child: const _VerifyEmailPage(),
    );
  }
}

class _VerifyEmailPage extends StatefulWidget {
  const _VerifyEmailPage({Key? key}) : super(key: key);

  @override
  _VerifyEmailPageState createState() => _VerifyEmailPageState();
}

class _VerifyEmailPageState extends State<_VerifyEmailPage> {
  get onProceedButton => () {
        GoRouter.of(context).goNamed('index');
      };

  get onRequestNewLinkButton => () {
        retrieveUserEmailAndSendVerificationCode();
      };

  get onGoBackButtonPressed => () {
        GoRouter.of(context).goNamed('home');
      };

  void retrieveUserEmailAndSendVerificationCode() {
    context.read<VerifyEmailNotifier>().retrieveEmailAddress(
        onResult: (Result<dz.Unit, String?> result) {
      result.maybeMap(
          onSuccess: (_) {
            sendVerificationCode();
          },
          orElse: () {});
    });
  }

  void sendVerificationCode() {
    context.read<VerifyEmailNotifier>().sendVerificationEmail(
        onResult: (Result<dz.Unit, dz.Unit> result) {
      result.maybeWhen(onSuccess: (_) {}, orElse: () {});
    });
  }

  void applyActionCode({required TextValue actionCode}) async {
    context.read<VerifyEmailNotifier>().applyVerificationCode(
          actionCode: actionCode,
          onResult: (Result<dz.Unit, dz.Unit> result) {
            result.maybeWhen(onSuccess: (_) {
              context.read<VerifyEmailNotifier>().loadingState =
                  LoadingState.successful;
            }, orElse: () {
              context.read<VerifyEmailNotifier>().loadingState =
                  LoadingState.failed;
            });
          },
        );
  }

  void checkActionCodeAndApplyActionCode() async {
    context.read<VerifyEmailNotifier>().checkVerificationCode(
          actionCode: context.read<VerifyEmailNotifier>().actionCode,
          onResult: (Result<dz.Unit, TextValue> result) {
            result.maybeWhen(onSuccess: (TextValue actionCode) {
              applyActionCode(actionCode: actionCode);
            }, orElse: () {
              context.read<VerifyEmailNotifier>().loadingState =
                  LoadingState.failed;
            });
          },
        );
  }

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      checkActionCodeAndApplyActionCode();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Responsive(
      mobile: _VerifyEmailView(this),
      tablet: _VerifyEmailView(this),
      desktop: _VerifyEmailView(this),
    );
  }
}

class _VerifyEmailView
    extends WidgetView<_VerifyEmailPage, _VerifyEmailPageState> {
  const _VerifyEmailView(_VerifyEmailPageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 48.0),
          child: LoadingAnimation(
            size: 120,
            state: context.watch<VerifyEmailNotifier>().loadingState,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 48),
          child: Text(
            context.watch<VerifyEmailNotifier>().isLoading
                ? AppLocalizations.of(context)!.verifyingEmail
                : context.watch<VerifyEmailNotifier>().isSuccessful
                    ? AppLocalizations.of(context)!.yourEmailHasBeenVerified
                    : AppLocalizations.of(context)!.unableToVerifyEmail,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ),
        const SizedBox(
          height: 52,
        ),
        Center(
          child: SizedBox(
            width: context.widthPct(0.6),
            child: context.watch<VerifyEmailNotifier>().isLoading
                ? const SizedBox.shrink()
                : context.watch<VerifyEmailNotifier>().isSuccessful
                    ? context.watch<VerifyEmailNotifier>().userInstanceExist
                        ? ElevatedButton(
                            onPressed: context
                                    .watch<VerifyEmailNotifier>()
                                    .isProcessing
                                ? null
                                : state.onProceedButton,
                            child: Text(AppLocalizations.of(context)!.proceed),
                          )
                        : ElevatedButton(
                            onPressed: context
                                    .watch<VerifyEmailNotifier>()
                                    .isProcessing
                                ? null
                                : state.onGoBackButtonPressed,
                            child: Text(AppLocalizations.of(context)!.goBack),
                          )
                    : context.watch<VerifyEmailNotifier>().userInstanceExist
                        ? TextButton(
                            onPressed: context
                                    .watch<VerifyEmailNotifier>()
                                    .isProcessing
                                ? null
                                : state.onRequestNewLinkButton,
                            child: Text(
                                AppLocalizations.of(context)!.requestNewLink),
                          )
                        : TextButton(
                            onPressed: context
                                    .watch<VerifyEmailNotifier>()
                                    .isProcessing
                                ? null
                                : state.onGoBackButtonPressed,
                            child: Text(AppLocalizations.of(context)!.goBack),
                          ),
          ),
        ),
      ],
    );
  }
}
