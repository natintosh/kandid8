import 'package:flutter/material.dart';
import 'package:kaykey/l10n/localization.dart';
import 'package:kaykey/presentation/notifiers/signup_notifier.dart';
import 'package:kaykey/presentation/widgets/loading_animation.dart';
import 'package:provider/provider.dart';

class SignUpForm extends StatelessWidget {
  final VoidCallback onSignUpButtonPressed;

  final TextEditingController firstNameController;
  final TextEditingController lastNameController;
  final TextEditingController emailController;
  final TextEditingController passwordController;
  final TextEditingController confirmPasswordController;

  const SignUpForm(
      {Key? key,
      required this.onSignUpButtonPressed,
      required this.firstNameController,
      required this.lastNameController,
      required this.emailController,
      required this.passwordController,
      required this.confirmPasswordController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 48.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              Expanded(
                child: ListTile(
                  contentPadding: EdgeInsets.zero,
                  title: Padding(
                    padding: const EdgeInsets.only(bottom: 16),
                    child: Text(
                      AppLocalizations.of(context)!.firstName,
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                  ),
                  subtitle: TextFormField(
                    controller: firstNameController,
                    validator: (String? value) => context
                        .read<SignUpNotifier>()
                        .firstNameValidator(value,
                            onData: () =>
                                AppLocalizations.of(context)!.requiredField),
                  ),
                ),
              ),
              const SizedBox(width: 16),
              Expanded(
                child: ListTile(
                  contentPadding: EdgeInsets.zero,
                  title: Padding(
                    padding: const EdgeInsets.only(bottom: 16),
                    child: Text(
                      AppLocalizations.of(context)!.lastName,
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                  ),
                  subtitle: TextFormField(
                    controller: lastNameController,
                    validator: (String? value) => context
                        .read<SignUpNotifier>()
                        .lastNameValidator(value,
                            onData: () =>
                                AppLocalizations.of(context)!.requiredField),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 8),
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Padding(
              padding: const EdgeInsets.only(bottom: 16),
              child: Text(
                AppLocalizations.of(context)!.emailAddress,
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ),
            subtitle: TextFormField(
              controller: emailController,
              validator: (String? value) => context
                  .read<SignUpNotifier>()
                  .emailAddressValidator(value,
                      onData: () =>
                          AppLocalizations.of(context)!.invalidEmailAddress),
            ),
          ),
          const SizedBox(height: 8),
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Padding(
              padding: const EdgeInsets.only(bottom: 16),
              child: Text(
                AppLocalizations.of(context)!.password,
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ),
            subtitle: TextFormField(
              controller: passwordController,
              obscureText: context.watch<SignUpNotifier>().obscurePassword,
              decoration: InputDecoration(
                suffixIcon: IconButton(
                    onPressed: context
                        .read<SignUpNotifier>()
                        .onPasswordIconButtonPressed,
                    icon: Icon(context.watch<SignUpNotifier>().obscurePassword
                        ? Icons.visibility
                        : Icons.visibility_off)),
              ),
              validator: (String? value) => context
                  .read<SignUpNotifier>()
                  .passwordValidator(value,
                      onData: () => AppLocalizations.of(context)!.weakPassword),
            ),
          ),
          const SizedBox(height: 8),
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Padding(
              padding: const EdgeInsets.only(bottom: 16),
              child: Text(
                AppLocalizations.of(context)!.confirmPassword,
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ),
            subtitle: TextFormField(
              controller: confirmPasswordController,
              obscureText:
                  context.watch<SignUpNotifier>().obscureConfirmPassword,
              decoration: InputDecoration(
                  suffixIcon: IconButton(
                      onPressed: context
                          .read<SignUpNotifier>()
                          .onConfirmPasswordIconButtonPressed,
                      icon: Icon(
                          context.watch<SignUpNotifier>().obscureConfirmPassword
                              ? Icons.visibility
                              : Icons.visibility_off))),
              validator: (String? value) => context
                  .read<SignUpNotifier>()
                  .confirmPasswordValidator(value,
                      onData: () =>
                          AppLocalizations.of(context)!.passwordDoNotMatch),
            ),
          ),
          const SizedBox(height: 20),
          SizedBox(
            width: double.infinity,
            child: ElevatedButton(
              onPressed: context.watch<SignUpNotifier>().isLoading
                  ? null
                  : onSignUpButtonPressed,
              child: context.watch<SignUpNotifier>().isLoading
                  ? const LoadingAnimation(size: 24)
                  : Text(AppLocalizations.of(context)!.signup),
            ),
          ),
        ],
      ),
    );
  }
}
