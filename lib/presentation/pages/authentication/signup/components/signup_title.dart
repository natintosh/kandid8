import 'package:flutter/material.dart';
import 'package:kaykey/common/themes/themes.dart';
import 'package:kaykey/l10n/localization.dart';

class SignUpTitle extends StatelessWidget {
  const SignUpTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 64.0),
          child: Text(
            AppLocalizations.of(context)!.title,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.onTitle.headline5!.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 12),
          child: Text(
            AppLocalizations.of(context)!.createAccount,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 48),
          child: Text(
            AppLocalizations.of(context)!.personalInfo,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
      ],
    );
  }
}
