import 'package:dartz/dartz.dart' as dz;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:kaykey/application/authentication/authentication.service.dart';
import 'package:kaykey/domain/common/result.dart';
import 'package:kaykey/infrastructure/authentication/authentication.facade.dart';
import 'package:kaykey/presentation/notifiers/signup_notifier.dart';
import 'package:kaykey/presentation/pages/authentication/signup/components/signin_text.dart';
import 'package:kaykey/presentation/pages/authentication/signup/components/signup_form.dart';
import 'package:kaykey/presentation/pages/authentication/signup/components/signup_title.dart';
import 'package:kaykey/presentation/widgets/display_error.dart';
import 'package:kaykey/presentation/widgets/responsive.dart';
import 'package:kaykey/presentation/widgets/widget_view.dart';
import 'package:provider/provider.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) {
        return SignUpNotifier(
          AuthenticationService(
              repository: AuthenticationFacadeImpl(
                  firebaseAuth: FirebaseAuth.instance)),
        );
      },
      child: const _SignUpPage(),
    );
  }
}

class _SignUpPage extends StatefulWidget {
  const _SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<_SignUpPage> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  late final TextEditingController firstNameController;
  late final TextEditingController lastNameController;
  late final TextEditingController emailAddressController;
  late final TextEditingController passwordController;
  late final TextEditingController confirmPasswordController;

  get signinTextGestureRecognizer =>
      TapGestureRecognizer()..onTap = onSinginButtonPressed;

  get onSignUpButtonPressed => () {
        if (formKey.currentState!.validate()) {
          context.read<SignUpNotifier>().createUserWithEmailAndPassword(
              onResult: (Result<dz.Unit, dz.Unit> result) {
            result.maybeWhen(onSuccess: (_) {
              goToEmailVerification();
            }, orElse: () {
              return;
            });
          });
        }
      };

  GestureTapCallback? get onSinginButtonPressed => goToSignIn;

  VoidCallback get goToSignIn => () {
        GoRouter.of(context).goNamed('signin');
      };

  VoidCallback get goToEmailVerification => () {
        GoRouter.of(context).goNamed('emailVerification', extra: {
          'emailAddress': context.read<SignUpNotifier>().emailAddress
        });
      };

  void firstNameListener() {
    context.read<SignUpNotifier>().firstName = firstNameController.text;
  }

  void lastNameListener() {
    context.read<SignUpNotifier>().lastName = lastNameController.text;
  }

  void emailAddressListener() {
    context.read<SignUpNotifier>().emailAddress = emailAddressController.text;
  }

  void passwordListener() {
    context.read<SignUpNotifier>().password = passwordController.text;
  }

  void confirmPasswordListener() {
    context.read<SignUpNotifier>().confirmPassword =
        confirmPasswordController.text;
  }

  @override
  void initState() {
    firstNameController = TextEditingController()
      ..addListener(firstNameListener);
    lastNameController = TextEditingController()..addListener(lastNameListener);
    emailAddressController = TextEditingController()
      ..addListener(emailAddressListener);
    passwordController = TextEditingController()..addListener(passwordListener);
    confirmPasswordController = TextEditingController()
      ..addListener(confirmPasswordListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: Form(
        key: formKey,
        child: Responsive(
          mobile: _MobileSignUpView(this),
          tablet: _TabletSignUpView(this),
          desktop: _DesktopSignUpView(this),
        ),
      ),
    );
  }
}

class _MobileSignUpView extends WidgetView<_SignUpPage, _SignUpPageState> {
  const _MobileSignUpView(_SignUpPageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      children: [
        const SignUpTitle(),
        SignUpForm(
          onSignUpButtonPressed: state.onSignUpButtonPressed,
          firstNameController: state.firstNameController,
          lastNameController: state.lastNameController,
          emailController: state.emailAddressController,
          passwordController: state.passwordController,
          confirmPasswordController: state.confirmPasswordController,
        ),
        context.watch<SignUpNotifier>().showError
            ? DisplayError(
                message: context.watch<SignUpNotifier>().errorMessage,
                onCancelButtonPressed:
                    context.watch<SignUpNotifier>().hideError,
              )
            : const SizedBox.shrink(),
        SigninText(
          signupTextGestureRecognizer: state.signinTextGestureRecognizer,
        ),
        const SizedBox(height: 60)
      ],
    );
  }
}

class _TabletSignUpView extends WidgetView<_SignUpPage, _SignUpPageState> {
  const _TabletSignUpView(_SignUpPageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 150),
      children: [
        const SignUpTitle(),
        SignUpForm(
          onSignUpButtonPressed: state.onSignUpButtonPressed,
          firstNameController: state.firstNameController,
          lastNameController: state.lastNameController,
          emailController: state.emailAddressController,
          passwordController: state.passwordController,
          confirmPasswordController: state.confirmPasswordController,
        ),
        context.watch<SignUpNotifier>().showError
            ? DisplayError(
                message: context.watch<SignUpNotifier>().errorMessage,
                onCancelButtonPressed:
                    context.watch<SignUpNotifier>().hideError,
              )
            : const SizedBox.shrink(),
        SigninText(
          signupTextGestureRecognizer: state.signinTextGestureRecognizer,
        ),
        const SizedBox(height: 60)
      ],
    );
  }
}

class _DesktopSignUpView extends WidgetView<_SignUpPage, _SignUpPageState> {
  const _DesktopSignUpView(_SignUpPageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 250.0),
      children: [
        const SignUpTitle(),
        SignUpForm(
          onSignUpButtonPressed: state.onSignUpButtonPressed,
          firstNameController: state.firstNameController,
          lastNameController: state.lastNameController,
          emailController: state.emailAddressController,
          passwordController: state.passwordController,
          confirmPasswordController: state.confirmPasswordController,
        ),
        context.watch<SignUpNotifier>().showError
            ? DisplayError(
                message: context.watch<SignUpNotifier>().errorMessage,
                onCancelButtonPressed:
                    context.watch<SignUpNotifier>().hideError,
              )
            : const SizedBox.shrink(),
        SigninText(
          signupTextGestureRecognizer: state.signinTextGestureRecognizer,
        ),
        const SizedBox(height: 60)
      ],
    );
  }
}
