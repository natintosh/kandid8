import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:kaykey/presentation/widgets/widget_view.dart';

class ErrorPage extends StatefulWidget {
  const ErrorPage({Key? key}) : super(key: key);

  @override
  _ErrorPageState createState() => _ErrorPageState();
}

class _ErrorPageState extends State<ErrorPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _ErrorPageView(this),
    );
  }
}

class _ErrorPageView extends WidgetView<ErrorPage, _ErrorPageState> {
  const _ErrorPageView(_ErrorPageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    String path = GoRouter.of(context).location;
    return Center(
      child: Text("Path $path does not exist"),
    );
  }
}
