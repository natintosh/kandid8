import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:kaykey/presentation/widgets/widget_view.dart';
import 'package:kaykey/common/themes/colors.dart';
import 'package:kaykey/l10n/localization.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  get onSigninButtonPressed => () {
        GoRouter.of(context).goNamed('signin');
      };

  get onSignupButtonPressed => () {
        GoRouter.of(context).goNamed('signup');
      };

  @override
  Widget build(BuildContext context) {
    return _HomePageView(this);
  }
}

class _HomePageView extends WidgetView<HomePage, _HomePageState> {
  const _HomePageView(_HomePageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          AppLocalizations.of(context)!.title,
          style: Theme.of(context)
              .textTheme
              .headline4!
              .copyWith(color: KaykeyColors.secondary),
        ),
        ButtonBar(
          alignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: state.onSigninButtonPressed,
              child: Text(AppLocalizations.of(context)!.signin),
            ),
            TextButton(
              onPressed: state.onSignupButtonPressed,
              child: Text(
                AppLocalizations.of(context)!.signup,
              ),
            )
          ],
        ),
      ],
    ));
  }
}
