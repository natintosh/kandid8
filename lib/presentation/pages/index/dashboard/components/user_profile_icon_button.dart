import 'package:flutter/material.dart';
import 'package:kaykey/common/themes/colors.dart';

class UserProfileIconButton extends StatelessWidget {
  const UserProfileIconButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const CircleAvatar(
          backgroundColor: KaykeyColors.background,
        ),
        const SizedBox(width: 12),
        Text(
          'Demo User',
          style: Theme.of(context)
              .textTheme
              .bodyText1!
              .copyWith(color: KaykeyColors.onPrimary),
        ),
      ],
    );
  }
}
