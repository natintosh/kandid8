import 'package:flutter/material.dart';
import 'package:kaykey/presentation/widgets/card_title.dart';

class HowItWorks extends StatelessWidget {
  const HowItWorks({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: CardTitle(
                icon: Icon(Icons.class_),
                title: 'How Kandid8 Works',
              ),
            ),
            const SizedBox(height: 16),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                'Learn how to get started with Kandid8',
                style: themeData.textTheme.bodyText2,
              ),
            ),
            const SizedBox(height: 16),
            ListTile(
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              leading: const CircleAvatar(
                child: Icon(Icons.drive_file_rename_outline_sharp),
              ),
              title: Text(
                'Create Test',
                style: themeData.textTheme.subtitle2,
              ),
              onTap: () {},
            ),
            ListTile(
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              leading: const CircleAvatar(
                child: Icon(Icons.group_add),
              ),
              title: Text(
                'Add Candidate',
                style: themeData.textTheme.subtitle2,
              ),
              onTap: () {},
            ),
            ListTile(
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              leading: const CircleAvatar(
                child: Icon(Icons.link),
              ),
              title: Text(
                'Send Links',
                style: themeData.textTheme.subtitle2,
              ),
              onTap: () {},
            ),
            ListTile(
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              leading: const CircleAvatar(
                child: Icon(Icons.analytics),
              ),
              title: Text(
                'Analyse Result',
                style: themeData.textTheme.subtitle2,
              ),
              onTap: () {},
            ),
          ],
        ),
      ),
    );
  }
}
