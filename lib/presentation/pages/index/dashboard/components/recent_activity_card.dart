import 'package:flutter/material.dart';
import 'package:kaykey/presentation/widgets/card_title.dart';
import 'package:kaykey/presentation/widgets/graph/graph_view.dart';

class RecentActivityCard extends StatelessWidget {
  const RecentActivityCard({Key? key, required this.value}) : super(key: key);

  final List<double> value;

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
              child: CardTitle(icon: Icon(Icons.show_chart), title: 'Insight'),
            ),
            const SizedBox(height: 32),
            SizedBox(
              height: 200,
              child: GraphViewWidget(
                values: value,
                color: themeData.primaryColorDark,
              ),
            )
          ],
        ),
      ),
    );
  }
}
