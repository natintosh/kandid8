import 'package:flutter/material.dart';
import 'package:kaykey/presentation/widgets/card_title.dart';

class SummaryCard extends StatelessWidget {
  const SummaryCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            const CardTitle(
              icon: Icon(Icons.receipt),
              title: 'Summary',
            ),
            Column(
              children: [
                const SizedBox(height: 24),
                SizedBox(
                  height: 84,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: const [
                      Flexible(
                        child: _SummaryItem(
                          value: '20',
                          label: 'Total Tests',
                        ),
                      ),
                      VerticalDivider(),
                      Flexible(
                        child: _SummaryItem(
                          value: '1600',
                          label: 'Total Candidates',
                        ),
                      ),
                      VerticalDivider(),
                      Flexible(
                        child: _SummaryItem(
                          value: '40',
                          label: 'Question Bank',
                        ),
                      ),
                      VerticalDivider(),
                      Flexible(
                        child: _SummaryItem(
                          value: '5',
                          label: 'Categories',
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _SummaryItem extends StatelessWidget {
  const _SummaryItem({
    Key? key,
    required this.value,
    required this.label,
  }) : super(key: key);

  final String value;
  final String label;

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return Column(
      children: [
        Text(
          value,
          style: themeData.textTheme.headline5!.copyWith(
              fontWeight: FontWeight.bold, color: themeData.primaryColorDark),
        ),
        const SizedBox(height: 8),
        Text(label,
            textAlign: TextAlign.center,
            softWrap: true,
            maxLines: 2,
            overflow: TextOverflow.fade,
            style: themeData.textTheme.subtitle2),
      ],
    );
  }
}
