import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:kaykey/presentation/pages/index/dashboard/components/how_it_works.dart';
import 'package:kaykey/presentation/pages/index/dashboard/components/recent_activity_card.dart';
import 'package:kaykey/presentation/pages/index/dashboard/components/summary_card.dart';
import 'package:kaykey/presentation/widgets/responsive.dart';
import 'package:kaykey/presentation/widgets/widget_view.dart';
import 'package:sized_context/sized_context.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const _DashboardPage();
  }
}

class _DashboardPage extends StatefulWidget {
  const _DashboardPage({Key? key}) : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<_DashboardPage> {
  final List<double> values =
      List.generate(7, (index) => math.Random().nextDouble() * 100.0);

  @override
  Widget build(BuildContext context) {
    return Responsive(
      mobile: _MobileDashboardView(this),
      tablet: _TabletDashboardView(this),
      desktop: _DesktopDashboadView(this),
    );
  }
}

class _MobileDashboardView
    extends WidgetView<_DashboardPage, _DashboardPageState> {
  const _MobileDashboardView(_DashboardPageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    // final navigationstate = NavigationLayout.of(context);

    ThemeData themeData = Theme.of(context);
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: context.widthPct(0.03)) +
          EdgeInsets.only(bottom: context.heightPct(0.2)),
      shrinkWrap: true,
      children: [
        const SizedBox(height: 16),
        // TextButton.icon(
        // onPressed: navigationstate!.onExpandNavigationBar,
        // icon: AnimatedIcon(
        //   icon: AnimatedIcons.menu_close,
        // progress: navigationstate.controller,
        //   ),
        //   label: Text(AppLocalizations.of(context)!.menu),
        // ),
        const SizedBox(height: 48),
        Text(
          'Welcome Joseph,',
          style: themeData.textTheme.headline6,
        ),
        const SizedBox(height: 48),
        const SummaryCard(),
        const SizedBox(
          height: 16,
        ),
        RecentActivityCard(
          value: state.values,
        ),
        const SizedBox(height: 16),
        const HowItWorks(),
      ],
    );
  }
}

class _TabletDashboardView
    extends WidgetView<_DashboardPage, _DashboardPageState> {
  const _TabletDashboardView(_DashboardPageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: context.widthPct(0.04)),
      child: ListView(
        primary: false,
        shrinkWrap: true,
        children: [
          const SizedBox(height: 48),
          Text(
            'Welcome Joseph,',
            style: themeData.textTheme.headline6,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 48.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  flex: 3,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SummaryCard(),
                        const SizedBox(
                          height: 16,
                        ),
                        RecentActivityCard(
                          value: state.values,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: context.widthPct(0.025)),
                Flexible(
                  flex: 2,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: const [
                        HowItWorks(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _DesktopDashboadView
    extends WidgetView<_DashboardPage, _DashboardPageState> {
  const _DesktopDashboadView(_DashboardPageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: context.widthPct(0.08)),
      child: ListView(
        primary: false,
        shrinkWrap: true,
        children: [
          const SizedBox(height: 48),
          Text(
            'Welcome Joseph,',
            style: themeData.textTheme.headline6,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 48.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  flex: 3,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SummaryCard(),
                        const SizedBox(
                          height: 16,
                        ),
                        RecentActivityCard(
                          value: state.values,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: context.widthPct(0.025)),
                Flexible(
                  flex: 2,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: const [
                        HowItWorks(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
