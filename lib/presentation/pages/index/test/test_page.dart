import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:kaykey/common/utils/extenstions.dart';
import 'package:kaykey/presentation/widgets/empty_animation.dart';
import 'package:kaykey/presentation/widgets/responsive.dart';
import 'package:kaykey/presentation/widgets/widget_view.dart';
import 'package:sized_context/sized_context.dart';

class TestPage extends StatelessWidget {
  const TestPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    log('Reached');
    return const _TestPage();
  }
}

class _TestPage extends StatefulWidget {
  const _TestPage({Key? key}) : super(key: key);

  @override
  __TestPageState createState() => __TestPageState();
}

class __TestPageState extends State<_TestPage> {
  @override
  Widget build(BuildContext context) {
    return Responsive(
        mobile: _DeskTopTestView(this),
        tablet: _DeskTopTestView(this),
        desktop: _DeskTopTestView(this));
  }

  void onCreateNewTestButtonPressed() {}
}

class _DeskTopTestView extends WidgetView<_TestPage, __TestPageState> {
  const _DeskTopTestView(__TestPageState state) : super(state);

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return ListView(
      shrinkWrap: true,
      padding: EdgeInsets.symmetric(horizontal: context.widthPct(0.05)) +
          EdgeInsets.only(
              top: context.heightPct(0.05), bottom: context.heightPct(0.15)),
      children: [
        Text(
          'Test',
          style: themeData.textTheme.headline6
              ?.copyWith(fontWeight: FontWeight.bold)
              .primary(context),
        ),
        const Divider(),
        const SizedBox(height: 24),
        Align(
          alignment: Alignment.centerRight,
          child: ElevatedButton(
            onPressed: state.onCreateNewTestButtonPressed,
            child: const Text('Create New Test'),
          ),
        ),
        const SizedBox(height: 24),
        Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          clipBehavior: Clip.antiAlias,
          child: Padding(
            padding: const EdgeInsets.all(42.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Recent Tests',
                  style: themeData.textTheme.subtitle1,
                ),
                const SizedBox(height: 18),
                const Divider(),
                SizedBox(
                  height: context.heightPct(0.3),
                  child: Center(
                    child: EmptyAnimation(
                      size: context.heightPct(0.27),
                    ),
                  ),
                ),
                const SizedBox(height: 24),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    'You do not have any test yet, '
                    'click on Create New Text to get started',
                    textAlign: TextAlign.center,
                    style: themeData.textTheme.bodyText2,
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
