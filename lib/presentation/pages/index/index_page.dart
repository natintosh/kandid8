import 'package:flutter/material.dart';
import 'package:kaykey/l10n/localization.dart';
import 'package:kaykey/presentation/pages/index/dashboard/dashboard_page.dart';
import 'package:kaykey/presentation/pages/index/test/test_page.dart';
import 'package:kaykey/presentation/widgets/navigation/navigation_layout.dart';
import 'package:kaykey/presentation/widgets/navigation/navigation_side_bar_item.dart';

class IndexPage extends StatelessWidget {
  const IndexPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const _IndexPage();
  }
}

class _IndexPage extends StatefulWidget {
  const _IndexPage({Key? key}) : super(key: key);

  @override
  __IndexPageState createState() => __IndexPageState();
}

class __IndexPageState extends State<_IndexPage> {
  int currentIndex = 0;

  get navigationSideBarTapped => (int index) {
        setState(() {
          currentIndex = index;
        });
      };

  get navigationItems => [
        NavigationSideBarItem(
          icon: const Icon(Icons.space_dashboard),
          label: AppLocalizations.of(context)!.dashboard,
        ),
        NavigationSideBarItem(
          icon: const Icon(Icons.document_scanner),
          label: AppLocalizations.of(context)!.tests,
        ),
        NavigationSideBarItem(
          icon: const Icon(Icons.groups),
          label: AppLocalizations.of(context)!.candidates,
        ),
        NavigationSideBarItem(
          icon: const Icon(Icons.insert_chart_sharp),
          label: AppLocalizations.of(context)!.reports,
        ),
        NavigationSideBarItem(
          icon: const Icon(Icons.notifications),
          label: AppLocalizations.of(context)!.notifications,
        ),
        NavigationSideBarItem(
          icon: const Icon(Icons.settings),
          label: AppLocalizations.of(context)!.settings,
        ),
      ];

  List<Widget> pages = const [
    DashboardPage(),
    TestPage(),
    SizedBox(),
    SizedBox(),
    SizedBox(),
    SizedBox(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NavigationLayout(
        currentIndex: currentIndex,
        items: navigationItems,
        onTap: navigationSideBarTapped,
        body: AnimatedSwitcher(
            child: pages[currentIndex],
            duration: const Duration(
              milliseconds: 300,
            )),
      ),
    );
  }
}
