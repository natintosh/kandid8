import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:kaykey/application/authentication/authentication.service.dart';
import 'package:kaykey/common/types.dart';
import 'package:kaykey/domain/authentication/authentication.facade.dart';
import 'package:kaykey/domain/common/failure.dart';
import 'package:kaykey/domain/common/result.dart';
import 'package:kaykey/domain/common/text.value.dart';
import 'package:kaykey/domain/common/value.dart';

class SignUpNotifier extends ChangeNotifier {
  final AuthenticationService _authenticationService;

  SignUpNotifier(this._authenticationService);

  ProcessingState _processingState = ProcessingState.completed;

  bool _showError = false;

  String _errorMessage = '';

  String _firstName = '';
  String _lastName = '';
  String _emailAddress = '';
  String _password = '';
  String _confirmPassword = '';

  bool _obscureConfirmPassword = true;
  bool _obscurePassword = true;

  ProcessingState get processingState => _processingState;

  set processingState(ProcessingState value) {
    _processingState = value;
    notifyListeners();
  }

  bool get showError => _showError;

  set showError(bool value) {
    _showError = value;
    notifyListeners();
  }

  String get errorMessage => _errorMessage;

  set errorMessage(String value) {
    _errorMessage = value;
    notifyListeners();
  }

  bool get isLoading => processingState == ProcessingState.loading;

  void createUserWithEmailAndPassword(
      {required ResultCallback<Unit, Unit> onResult}) async {
    processingState = ProcessingState.loading;

    TextValue displayName = TextValue(firstName + ' ' + lastName);
    EmailAddressValue emailAddress = EmailAddressValue(this.emailAddress);
    PasswordValue password = PasswordValue(this.password);

    Result<Failure<CreateUserErrorCode>, UserCredential> result =
        await _authenticationService.createUserWithEmailAndPassword(
            displayName: displayName,
            emailAddress: emailAddress,
            password: password);

    result.when(onSuccess: (UserCredential success) {
      onResult.call(Result.onSuccess(value: unit));
    }, onFailure: (Failure<CreateUserErrorCode> failure) {
      showError = true;
      errorMessage = failure.message!;
      onResult.call(Result.onFailure(value: unit));
    });
    processingState = ProcessingState.completed;
  }

  String? firstNameValidator(String? value,
      {required DataCallback<String> onData}) {
    return TextValue(value!).value.fold(
          (l) => l.error == TextValueError.emptyText ? onData.call() : null,
          (r) => null,
        );
  }

  String? lastNameValidator(String? value,
      {required DataCallback<String> onData}) {
    return TextValue(value!).value.fold(
        (l) => l.error == TextValueError.emptyText ? onData.call() : null,
        (r) => null);
  }

  String? emailAddressValidator(String? value,
      {required DataCallback<String> onData}) {
    return EmailAddressValue(value!).value.fold(
        (l) => l.error == EmailAddressValueError.invalidEmail
            ? onData.call()
            : null,
        (r) => null);
  }

  String? passwordValidator(String? value,
      {required DataCallback<String> onData}) {
    return PasswordValue(value!).value.fold(
        (l) => l.error == PasswordValueError.invalidPassword
            ? onData.call()
            : null,
        (r) => null);
  }

  String? confirmPasswordValidator(String? value,
      {required DataCallback<String> onData}) {
    return value!.isEmpty
        ? null
        : value != password
            ? onData.call()
            : null;
  }

  String get lastName => _lastName;

  set lastName(String value) {
    _lastName = value;
    notifyListeners();
  }

  String get firstName => _firstName;

  set firstName(String value) {
    _firstName = value;
    notifyListeners();
  }

  String get emailAddress => _emailAddress;

  set emailAddress(String value) {
    _emailAddress = value;
    notifyListeners();
  }

  String get password => _password;

  set password(String value) {
    _password = value;
    notifyListeners();
  }

  String get confirmPassword => _confirmPassword;

  set confirmPassword(String value) {
    _confirmPassword = value;
    notifyListeners();
  }

  void hideError() {
    showError = false;
    notifyListeners();
  }

  bool get obscurePassword => _obscurePassword;

  set obscurePassword(bool value) {
    _obscurePassword = value;
    notifyListeners();
  }

  void onPasswordIconButtonPressed() {
    obscurePassword = !obscurePassword;
  }

  bool get obscureConfirmPassword => _obscureConfirmPassword;

  set obscureConfirmPassword(bool value) {
    _obscureConfirmPassword = value;
    notifyListeners();
  }

  void onConfirmPasswordIconButtonPressed() {
    obscureConfirmPassword = !obscureConfirmPassword;
  }
}
