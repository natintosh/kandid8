import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:kaykey/application/authentication/authentication.service.dart';
import 'package:kaykey/common/types.dart';
import 'package:kaykey/domain/common/result.dart';

class EmailVerificationNotifier extends ChangeNotifier {
  final AuthenticationService _authenticationService;

  EmailVerificationNotifier(BuildContext context, this._authenticationService);

  ProcessingState _processingState = ProcessingState.completed;

  String _emailAddress = '';

  ProcessingState get processingState => _processingState;

  set processingState(ProcessingState value) {
    _processingState = value;
    notifyListeners();
  }

  set emailAddress(String? value) {
    _emailAddress = value ?? '';
    notifyListeners();
  }

  String? get emailAddress => _emailAddress;

  bool get isRetrievingEmailAddress =>
      emailAddress!.isEmpty && processingState == ProcessingState.loading;

  bool get canSendVerificationEmail =>
      !isRetrievingEmailAddress && emailAddress!.isNotEmpty;

  void retrieveEmailAddress(
      {required ResultCallback<Unit, String?> onResult}) async {
    processingState = ProcessingState.loading;

    Result<Unit, User?> result = await _authenticationService.getFirebaseUser();

    result.when(onSuccess: (User? user) {
      onResult.call(Result.onSuccess(value: user!.email));
    }, onFailure: (_) {
      onResult.call(Result.onFailure(value: unit));
    });

    processingState = ProcessingState.completed;
  }

  void sendVerificationEmail(
      {required ResultCallback<Unit, Unit> onResult}) async {
    processingState = ProcessingState.loading;

    Result result = await _authenticationService.sendEmailVerificationCode();

    result.when(onSuccess: (_) {
      onResult.call(Result.onSuccess(value: unit));
    }, onFailure: (_) {
      onResult.call(Result.onFailure(value: unit));
    });

    processingState = ProcessingState.completed;
  }

  void checkUserVerificationStatus(
      {required ResultCallback<Unit, Unit> onResult}) async {
    processingState = ProcessingState.loading;

    Result<Unit, Unit> result = await _authenticationService.isUserVerified();

    result.when(onSuccess: (_) {
      onResult.call(Result.onSuccess(value: unit));
    }, onFailure: (_) {
      onResult.call(Result.onFailure(value: unit));
    });

    processingState = ProcessingState.completed;
  }
}
