import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:kaykey/application/authentication/authentication.service.dart';
import 'package:kaykey/common/types.dart';
import 'package:kaykey/domain/authentication/authentication.facade.dart';
import 'package:kaykey/domain/common/failure.dart';
import 'package:kaykey/domain/common/result.dart';
import 'package:kaykey/domain/common/text.value.dart';
import 'package:kaykey/presentation/widgets/loading_animation.dart';

class VerifyEmailNotifier extends ChangeNotifier {
  final AuthenticationService _authenticationService;

  VerifyEmailNotifier(BuildContext context, this._authenticationService);

  ProcessingState processingState = ProcessingState.completed;

  LoadingState _loadingState = LoadingState.load;

  TextValue _actionCode = TextValue('');

  LoadingState get loadingState => _loadingState;

  set loadingState(LoadingState value) {
    _loadingState = value;

    notifyListeners();
  }

  bool _userInstanceExist = false;

  bool get userInstanceExist => _userInstanceExist;

  set userInstanceExist(bool value) {
    _userInstanceExist = value;
    notifyListeners();
  }

  bool get isLoading => loadingState == LoadingState.load;

  bool get isSuccessful => loadingState == LoadingState.successful;

  bool get isProcessing => processingState == ProcessingState.loading;

  TextValue get actionCode => _actionCode;

  set actionCode(TextValue value) {
    _actionCode = value;
    notifyListeners();
  }

  void checkVerificationCode(
      {required TextValue actionCode,
      required ResultCallback<Unit, TextValue> onResult}) async {
    Result<Failure<ActionCodeErrorCode>, ActionCodeInfo> result =
        await _authenticationService.checkActionCode(actionCode: actionCode);

    result.when(onSuccess: (ActionCodeInfo actionCodeInfo) {
      onResult.call(Result.onSuccess(value: actionCode));
    }, onFailure: (Failure<ActionCodeErrorCode> error) {
      onResult.call(Result.onFailure(value: unit));
    });
  }

  void applyVerificationCode(
      {required TextValue actionCode,
      required ResultCallback<Unit, Unit> onResult}) async {
    Result<Failure<ActionCodeErrorCode>, Unit> result =
        await _authenticationService.applyActionCode(actionCode: actionCode);

    result.when(onSuccess: (_) {
      onResult.call(Result.onSuccess(value: unit));
    }, onFailure: (Failure<ActionCodeErrorCode> error) {
      onResult.call(Result.onFailure(value: unit));
    });
  }

  void checkForUserInstance() async {
    Result<Unit, User?> result = await _authenticationService.getFirebaseUser();

    result.when(onSuccess: (User? user) {
      userInstanceExist = true;
    }, onFailure: (_) {
      userInstanceExist = false;
    });
  }

  void retrieveEmailAddress(
      {required ResultCallback<Unit, String?> onResult}) async {
    processingState = ProcessingState.loading;

    Result<Unit, User?> result = await _authenticationService.getFirebaseUser();

    result.when(onSuccess: (User? user) {
      onResult.call(Result.onSuccess(value: user!.email));
    }, onFailure: (_) {
      onResult.call(Result.onFailure(value: unit));
    });

    processingState = ProcessingState.completed;
  }

  void sendVerificationEmail(
      {required ResultCallback<Unit, Unit> onResult}) async {
    processingState = ProcessingState.loading;

    Result result = await _authenticationService.sendEmailVerificationCode();

    result.when(onSuccess: (_) {
      onResult.call(Result.onSuccess(value: unit));
    }, onFailure: (_) {
      onResult.call(Result.onFailure(value: unit));
    });

    processingState = ProcessingState.completed;
  }
}
