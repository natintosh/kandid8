import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:kaykey/application/authentication/authentication.service.dart';
import 'package:kaykey/common/types.dart';
import 'package:kaykey/domain/authentication/authentication.facade.dart';
import 'package:kaykey/domain/common/failure.dart';
import 'package:kaykey/domain/common/result.dart';
import 'package:kaykey/domain/common/text.value.dart';
import 'package:kaykey/domain/common/value.dart';

class SignInNotifier extends ChangeNotifier {
  final AuthenticationService _authenticationService;

  SignInNotifier(this._authenticationService);

  ProcessingState _processingState = ProcessingState.completed;

  bool _showError = false;

  String _errorMessage = '';

  bool _obscurePassword = true;

  bool get obscurePassword => _obscurePassword;

  String _emailAddress = '';

  String _password = '';

  bool get showError => _showError;

  set showError(bool value) {
    _showError = value;
    notifyListeners();
  }

  String get errorMessage => _errorMessage;

  set errorMessage(String value) {
    _errorMessage = value;
    notifyListeners();
  }

  ProcessingState get processingState => _processingState;

  set processingState(ProcessingState value) {
    _processingState = value;
    notifyListeners();
  }

  bool get isProcessing => processingState == ProcessingState.loading;

  String get password => _password;

  set password(String value) {
    _password = value;
    notifyListeners();
  }

  String get emailAddress => _emailAddress;

  set emailAddress(String value) {
    _emailAddress = value;
    notifyListeners();
  }

  set obscurePassword(bool value) {
    _obscurePassword = value;
    notifyListeners();
  }

  String? emailAddressValidator(String? value,
      {required DataCallback<String> onData}) {
    return EmailAddressValue(value!).value.fold(
        (l) => l.error == EmailAddressValueError.invalidEmail
            ? onData.call()
            : null,
        (r) => null);
  }

  String? passwordValidator(String? value,
      {required DataCallback<String> onData}) {
    return TextValue(value!).value.fold(
        (l) => l.error == TextValueError.emptyText ? onData.call() : null,
        (r) => null);
  }

  void signInWithEmailAndPassword(
      {required ResultCallback<Unit, Unit> onResult,
      required ValueDataCallback<String, SignInErrorCode> onData}) async {
    processingState = ProcessingState.loading;

    EmailAddressValue emailAddress = EmailAddressValue(this.emailAddress);
    PasswordValue password = PasswordValue(this.password);

    Result<Failure<SignInErrorCode>, UserCredential> result =
        await _authenticationService.signInWithEmailAndPassword(
            emailAddress: emailAddress, password: password);

    result.when(onSuccess: (UserCredential value) {
      onResult.call(Result.onSuccess(value: unit));
    }, onFailure: (Failure<SignInErrorCode> value) {
      showError = true;
      if (value.error == SignInErrorCode.userDisabled) {
        errorMessage = value.message!;
      } else {
        errorMessage = onData.call(value.error);
      }
      onResult.call(Result.onFailure(value: unit));
    });

    processingState = ProcessingState.completed;
  }

  void signInWithGoogle(
      {required ResultCallback<Unit, Unit> onResult,
      required ValueDataCallback<String, SignInErrorCode> onData}) async {
    Result<Failure<SignInErrorCode>, UserCredential> result =
        await _authenticationService.signInWithGoogle();

    result.when(onSuccess: (UserCredential value) {
      onResult.call(Result.onSuccess(value: unit));
    }, onFailure: (Failure<SignInErrorCode> value) {
      showError = true;
      if (value.error == SignInErrorCode.userDisabled) {
        errorMessage = value.message!;
      } else {
        errorMessage = onData.call(value.error);
      }
      onResult.call(Result.onFailure(value: unit));
    });
  }

  void hideError() {
    showError = false;
  }

  void onPasswordIconButtonPressed() {
    obscurePassword = !obscurePassword;
  }
}
