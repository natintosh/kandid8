import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:kaykey/common/device/device.dart';
import 'package:kaykey/common/themes/colors.dart';
import 'package:kaykey/presentation/widgets/responsive.dart';

class DisplayError extends StatefulWidget {
  final String message;
  final VoidCallback onCancelButtonPressed;

  const DisplayError(
      {Key? key, required this.message, required this.onCancelButtonPressed})
      : super(key: key);
  @override
  _DisplayErrorState createState() => _DisplayErrorState();
}

class _DisplayErrorState extends State<DisplayError> {
  @override
  Widget build(BuildContext context) {
    return Builder(builder: (_) {
      bool isWideScreen = Device.isDesktop ||
          (Device.isWeb &&
              (Responsive.isDesktop(context) || Responsive.isTablet(context)));

      if (!isWideScreen) {
        WidgetsBinding.instance!.addPostFrameCallback((_) {
          ScaffoldMessenger.of(context).clearSnackBars();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            action: SnackBarAction(
                label: 'Close',
                onPressed: () {
                  widget.onCancelButtonPressed.call();
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                }),
            content: Text(
              widget.message,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(color: KaykeyColors.onError),
            ),
            backgroundColor: KaykeyColors.error,
          ));
        });
        return const SizedBox.shrink();
      }
      return Container(
        padding: const EdgeInsets.all(16.0),
        margin: const EdgeInsets.symmetric(vertical: 24.0),
        decoration: ShapeDecoration(
          color: KaykeyColors.error,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4.0),
          ),
        ),
        child: Row(
          children: [
            Expanded(
              child: Text(
                widget.message,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: KaykeyColors.onError),
              ),
            ),
            CloseButton(
              color: KaykeyColors.onError,
              onPressed: widget.onCancelButtonPressed,
            )
          ],
        ),
      );
    });
  }
}
