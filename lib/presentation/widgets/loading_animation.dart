import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kaykey/presentation/widgets/widget_view.dart';
import 'package:rive/rive.dart';

enum LoadingState { load, successful, failed }

class LoadingAnimation extends StatefulWidget {
  final double size;
  final LoadingState state;

  const LoadingAnimation({
    Key? key,
    this.size = 48,
    this.state = LoadingState.load,
  }) : super(key: key);

  @override
  _LoadingAnimationState createState() => _LoadingAnimationState();
}

class _LoadingAnimationState extends State<LoadingAnimation> {
  final String assetName = 'assets/rive/loading_indicator.riv';

  final String stateMachineName = 'loader_state';

  Artboard? artboard;

  StateMachineController? stateMachineController;

  SMIInput<bool>? loadingTrigger, successfulTriger, failedTrigger;

  void initializeRiveAnimation() async {
    try {
      final ByteData data = await rootBundle.load(assetName);

      final RiveFile riveFile = RiveFile.import(data);

      final Artboard artboard = riveFile.mainArtboard;

      stateMachineController =
          StateMachineController.fromArtboard(artboard, stateMachineName);

      if (stateMachineController != null) {
        artboard.addController(stateMachineController!);

        loadingTrigger = stateMachineController!.findInput<bool>('load');
        successfulTriger =
            stateMachineController!.findInput<bool>('successful');
        failedTrigger = stateMachineController!.findInput<bool>('failed');
      }

      setState(() {
        this.artboard = artboard;
      });

      loadingTrigger!.change(true);
    } on Exception catch (e) {
      log('$e');
    }
  }

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      initializeRiveAnimation();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return artboard == null
        ? SizedBox(width: widget.size, height: widget.size)
        : _LoadingAnimationView(this);
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void dispose() {
    stateMachineController?.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant LoadingAnimation oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.state != widget.state) {
      switch (widget.state) {
        case LoadingState.successful:
          loadingTrigger!.change(false);
          failedTrigger!.change(false);
          successfulTriger!.change(true);
          break;
        case LoadingState.failed:
          loadingTrigger!.change(false);
          successfulTriger!.change(false);
          failedTrigger!.change(true);
          break;
        default:
          if (!stateMachineController!.isActive &&
              loadingTrigger!.value == false) {
            artboard!.removeController(stateMachineController!);
            artboard!.addController(stateMachineController!);

            successfulTriger!.change(false);
            failedTrigger!.change(false);
            loadingTrigger!.change(true);
          }
      }
    }
  }
}

class _LoadingAnimationView
    extends WidgetView<LoadingAnimation, _LoadingAnimationState> {
  const _LoadingAnimationView(_LoadingAnimationState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.size,
      height: widget.size,
      child: Rive(
        artboard: state.artboard!,
      ),
    );
  }
}
