import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';

class GraphViewWidget extends LeafRenderObjectWidget {
  const GraphViewWidget({
    Key? key,
    required this.values,
    required this.color,
  }) : super(key: key);

  final List<double> values;
  final Color color;

  @override
  RenderObject createRenderObject(BuildContext context) {
    return RenderGraphViewWidget(values: values, color: color);
  }

  @override
  void updateRenderObject(
      BuildContext context, covariant RenderGraphViewWidget renderObject) {
    renderObject.values = values;
  }
}

class RenderGraphViewWidget extends RenderBox {
  RenderGraphViewWidget({required List<double> values, required Color color})
      : _values = values,
        _maxValue = values.reduce(max),
        _color = color;

  List<double> _values;

  double _maxValue;

  Color _color;

  List<double> get values {
    return _values;
  }

  Color get color {
    return _color;
  }

  set color(Color color) {
    if (this.color == color) return;
    _color = color;
    markNeedsPaint();
    markNeedsLayout();
  }

  set values(List<double> values) {
    if (this.values == values) return;
    _values = values;
    _maxValue = values.reduce(max);
    markNeedsPaint();
    markNeedsLayout();
  }

  @override
  bool get isRepaintBoundary => true;

  @override
  Size computeDryLayout(BoxConstraints constraints) {
    return _computeSize(constraints);
  }

  @override
  void performLayout() {
    size = _computeSize(constraints);
  }

  Size _computeSize(BoxConstraints constraints) {
    return constraints.constrain(
        Size(_initialPreferredSize.width, _initialPreferredSize.height));
  }

  Size get _initialPreferredSize {
    final maxSize = constraints.biggest;
    return Size(maxSize.width, maxSize.height);
  }

  double _interpolate(
      {required double input,
      double inputMax = 1,
      double inputMin = 0,
      double outputMax = 1,
      double outputMin = 0}) {
    return outputMin +
        ((outputMax - outputMin) / (inputMax - inputMin)) * (input - inputMin);
  }

  @override
  void paint(PaintingContext context, Offset offset) {
    final canvas = context.canvas;
    final viewportRect = Rect.fromLTWH(
      offset.dx,
      offset.dy,
      size.width,
      size.height,
    );

    final displacement = viewportRect.width / 6;

    final bottomPadding = viewportRect.height * 0.1;

    final width = viewportRect.width;
    final height = viewportRect.height;

    // draw curve line

    final controlPoints = <Offset>[];

    for (int i = 0; i < values.length; i++) {
      double y = height -
          _interpolate(
            input: values[i],
            inputMax: _maxValue + bottomPadding,
            outputMax: height,
          );

      double x0 = (displacement * 1) * i;
      controlPoints.add(Offset(x0, height - y));
    }

    final spline = CatmullRomSpline(controlPoints).generateSamples();

    final curvePath = Path()
      ..moveTo(controlPoints.first.dx, controlPoints.first.dy);

    final shadedCurvePath = Path()
      ..moveTo(
        controlPoints.first.dx,
        height - bottomPadding,
      );

    for (final sample in spline) {
      curvePath.lineTo(sample.value.dx, sample.value.dy);
      shadedCurvePath.lineTo(sample.value.dx, sample.value.dy);
    }

    shadedCurvePath.lineTo(controlPoints.last.dx, height - bottomPadding);

    final fillCurvePaint = Paint()
      ..style = PaintingStyle.fill
      ..shader = LinearGradient(
        colors: <Color>[
          color.withOpacity(0.6),
          color.withOpacity(0.5),
          color.withOpacity(0.3),
          color.withOpacity(0.1),
          color.withOpacity(0.0),
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ).createShader(shadedCurvePath.getBounds());

    final curvePaint = Paint()
      ..style = PaintingStyle.stroke
      ..color = color
      ..strokeWidth = 4
      ..strokeCap = StrokeCap.round;

    canvas.drawPath(shadedCurvePath, fillCurvePaint);
    canvas.drawPath(curvePath, curvePaint);
  }
}
