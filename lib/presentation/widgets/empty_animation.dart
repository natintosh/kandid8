import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rive/rive.dart';

class EmptyAnimation extends StatefulWidget {
  const EmptyAnimation({Key? key, required this.size}) : super(key: key);

  final double size;

  @override
  _EmptyAnimationState createState() => _EmptyAnimationState();
}

class _EmptyAnimationState extends State<EmptyAnimation> {
  final String assetName = 'assets/rive/empty.riv';

  Artboard? artboard;

  void initializeRiveAnimation() async {
    try {
      final ByteData data = await rootBundle.load(assetName);
      final RiveFile riveFile = RiveFile.import(data);

      final Artboard? artboard = riveFile.artboardByName('Empty Artboard');

      setState(() {
        this.artboard = artboard;
      });
    } on Exception catch (e) {
      log('$e');
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      initializeRiveAnimation();
    });
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.square(
      dimension: widget.size,
      child: artboard == null
          ? const SizedBox.shrink()
          : Rive(
              artboard: artboard!,
            ),
    );
  }
}
