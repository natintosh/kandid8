import 'package:flutter/material.dart';
import 'package:kaykey/common/themes/colors.dart';
import 'package:kaykey/common/utils/extenstions.dart';
import 'package:kaykey/presentation/widgets/navigation/navigation_side_bar.dart';
import 'package:kaykey/presentation/widgets/navigation/navigation_side_bar_item.dart';
import 'package:kaykey/presentation/widgets/responsive.dart';
import 'package:kaykey/presentation/widgets/widget_view.dart';

class NavigationLayout extends StatefulWidget {
  const NavigationLayout({
    Key? key,
    required this.currentIndex,
    required this.items,
    required this.onTap,
    required this.body,
  }) : super(key: key);

  final Widget? body;
  final int currentIndex;
  final ValueChanged<int> onTap;
  final List<NavigationSideBarItem> items;

  @override
  NavigationLayoutState createState() => NavigationLayoutState();

  static NavigationLayoutState? of(BuildContext context) {
    final state = context.findAncestorStateOfType<NavigationLayoutState>();

    if (state != null) {
      return state;
    }

    FlutterError('Could not find ancestor state of type `NavigationLayoutState`'
        'Try wrapping the widget with NavigationLayout');
  }
}

class NavigationLayoutState extends State<NavigationLayout>
    with SingleTickerProviderStateMixin {
  final double kHiddenWidth = 0;
  final double kCollapsedWidth = 85;
  final double kExpandedWidth = 210;

  late final AnimationController controller;

  bool navigationBarExpanded = true;

  void onExpandNavigationBar() {
    if (navigationBarExpanded) {
      controller.stop();
      controller.forward(from: controller.value);
    } else {
      controller.stop();
      controller.reverse(from: controller.value);
    }
    setState(() {
      navigationBarExpanded = !navigationBarExpanded;
    });
  }

  Widget buildBody(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    return Material(
      color: themeData.scaffoldBackgroundColor,
      child: widget.body,
    );
  }

  @override
  void initState() {
    controller =
        AnimationController(vsync: this, duration: kThemeAnimationDuration);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Responsive(
        mobile: _MobileNavigationLayout(this),
        tablet: _TabletNavigationLayout(this),
        desktop: _DesktopNavigationLayout(this));
  }
}

class _MobileNavigationLayout
    extends WidgetView<NavigationLayout, NavigationLayoutState> {
  const _MobileNavigationLayout(NavigationLayoutState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(color: KaykeyColors.primary),
          child: Padding(
            padding: const EdgeInsets.only(
                top: 16.0, right: 24.0, bottom: 16.0, left: 16.0),
            child: NavigationSideBar(
              controller: state.controller,
              onExpandNavigationBar: state.onExpandNavigationBar,
              currentIndex: widget.currentIndex,
              onTap: widget.onTap,
              items: widget.items,
            ),
          ),
        ),
        AnimatedBuilder(
          animation: state.controller,
          builder: (BuildContext context, Widget? child) {
            double position = state.controller.value.interpolate(
                from: state.kHiddenWidth,
                to: state.kExpandedWidth,
                max: 1,
                min: 0);

            return Transform(
              transform: Matrix4.identity()..translate(position),
              child: child!,
            );
          },
          child: state.buildBody(context),
        ),
      ],
    );
  }
}

class _TabletNavigationLayout
    extends WidgetView<NavigationLayout, NavigationLayoutState> {
  const _TabletNavigationLayout(NavigationLayoutState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(color: KaykeyColors.primary),
          child: Padding(
            padding: const EdgeInsets.only(
                top: 16.0, right: 24.0, bottom: 16.0, left: 16.0),
            child: NavigationSideBar(
              controller: state.controller,
              onExpandNavigationBar: state.onExpandNavigationBar,
              currentIndex: widget.currentIndex,
              onTap: widget.onTap,
              items: widget.items,
            ),
          ),
        ),
        AnimatedBuilder(
          animation: state.controller,
          builder: (BuildContext context, Widget? child) {
            double position = state.controller.value.interpolate(
                from: state.kHiddenWidth,
                to: state.kExpandedWidth - state.kCollapsedWidth,
                max: 1,
                min: 0);

            return Positioned.fill(
              left: 85,
              child: Transform(
                transform: Matrix4.identity()..translate(position),
                child: child!,
              ),
            );
          },
          child: state.buildBody(context),
        ),
      ],
    );
  }
}

class _DesktopNavigationLayout
    extends WidgetView<NavigationLayout, NavigationLayoutState> {
  const _DesktopNavigationLayout(NavigationLayoutState state) : super(state);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: KaykeyColors.primary,
          child: Padding(
            padding: const EdgeInsets.only(
                top: 16.0, right: 24.0, bottom: 16.0, left: 16.0),
            child: NavigationSideBar(
              controller: state.controller,
              onExpandNavigationBar: state.onExpandNavigationBar,
              currentIndex: widget.currentIndex,
              onTap: widget.onTap,
              items: widget.items,
            ),
          ),
        ),
        AnimatedBuilder(
          animation: state.controller,
          builder: (BuildContext context, Widget? child) {
            double position = state.controller.value.interpolate(
                from: state.kCollapsedWidth,
                to: state.kExpandedWidth,
                max: 1,
                min: 0);
            return Positioned.fill(
              left: position,
              child: child!,
            );
          },
          child: state.buildBody(context),
        ),
      ],
    );
  }
}
