import 'package:flutter/material.dart';
import 'package:kaykey/common/themes/colors.dart';
import 'package:kaykey/common/utils/extenstions.dart';
import 'package:kaykey/l10n/localization.dart';
import 'package:kaykey/presentation/widgets/navigation/navigation_side_bar_item.dart';

class _NavigationSideBarTile extends StatefulWidget {
  const _NavigationSideBarTile(
      {Key? key,
      required this.item,
      required this.selected,
      required this.animation,
      required this.sizeAnimation,
      this.onTap,
      required this.colorTween,
      required this.iconSize,
      required this.selectedIconTheme,
      required this.unselectedIconTheme})
      : super(key: key);

  final NavigationSideBarItem item;
  final bool selected;
  final AnimationController sizeAnimation;
  final Animation<double> animation;
  final VoidCallback? onTap;

  final ColorTween colorTween;
  final double iconSize;
  final IconThemeData? selectedIconTheme;
  final IconThemeData? unselectedIconTheme;

  @override
  _NavigationSideBarTileState createState() => _NavigationSideBarTileState();
}

class _NavigationSideBarTileState extends State<_NavigationSideBarTile> {
  final double maxWidth = 180;
  final double minWidth = 56;

  late Animation<double> hide;

  late Animation<double> hideOpacity;

  MaterialPropertyResolver<Color> get getRippleColor =>
      (Set<MaterialState> states) {
        return KaykeyColors.primaryVariant.withOpacity(0.2);
      };

  void _initAnimations() {
    hide = Tween<double>(begin: 0, end: 1).animate(CurvedAnimation(
        parent: widget.sizeAnimation, curve: const Interval(0, 0.8)));
    hideOpacity = Tween<double>(begin: 0, end: 1).animate(CurvedAnimation(
        parent: widget.sizeAnimation, curve: const Interval(0.8, 1)));
  }

  @override
  void initState() {
    _initAnimations();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: widget.sizeAnimation,
      builder: (BuildContext context, Widget? child) {
        double width = hide.value
            .interpolate(from: minWidth, to: maxWidth, max: 1, min: 0);

        double opacity = hideOpacity.value;

        bool isHidden = opacity == 0;

        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Container(
            width: width,
            clipBehavior: Clip.antiAlias,
            decoration: ShapeDecoration(
              color: widget.selected
                  ? KaykeyColors.background
                  : KaykeyColors.background.withOpacity(0.2),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
              ),
            ),
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: widget.onTap,
                overlayColor: MaterialStateColor.resolveWith(getRippleColor),
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      _TileIcon(
                          item: widget.item,
                          selected: widget.selected,
                          // TODO: Update animation to individual select animation
                          animation: widget.animation,
                          colorTween: widget.colorTween,
                          iconSize: widget.iconSize,
                          selectedIconTheme: widget.selectedIconTheme,
                          unselectedIconTheme: widget.unselectedIconTheme),
                      isHidden
                          ? const SizedBox.shrink()
                          : const SizedBox(width: 8.0),
                      isHidden
                          ? const SizedBox.shrink()
                          : Opacity(
                              opacity: opacity,
                              child: Text(
                                widget.item.label,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(
                                        color: widget.selected
                                            ? KaykeyColors.onBackground
                                            : KaykeyColors.onPrimary),
                              ),
                            ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
      child: Container(),
    );
  }
}

class _TileIcon extends StatelessWidget {
  const _TileIcon({
    Key? key,
    required this.item,
    required this.selected,
    required this.animation,
    required this.colorTween,
    required this.iconSize,
    required this.selectedIconTheme,
    required this.unselectedIconTheme,
  }) : super(key: key);

  final ColorTween colorTween;
  final double iconSize;
  final NavigationSideBarItem item;
  final bool selected;
  final IconThemeData? selectedIconTheme;
  final IconThemeData? unselectedIconTheme;
  final Animation<double> animation;

  @override
  Widget build(BuildContext context) {
    final Color? color = colorTween.evaluate(animation);
    final IconThemeData defaultIconTheme = IconThemeData(
      color: color,
      size: iconSize,
    );

    final IconThemeData iconThemeData = IconThemeData.lerp(
      defaultIconTheme.merge(selectedIconTheme),
      defaultIconTheme.merge(unselectedIconTheme),
      animation.value,
    );

    return IconTheme(
        data: iconThemeData, child: selected ? item.activeIcon : item.icon);
  }
}

class _NavigationSideBarActionTile extends StatefulWidget {
  const _NavigationSideBarActionTile({
    Key? key,
    required this.item,
    required this.animation,
    required this.onTap,
  }) : super(key: key);

  final Animation<double> animation;

  final VoidCallback? onTap;

  final NavigationSideBarItem item;

  @override
  State<_NavigationSideBarActionTile> createState() {
    return _NavigationSideBarActionTileState();
  }
}

class _NavigationSideBarActionTileState
    extends State<_NavigationSideBarActionTile> {
  final double maxWidth = 180;
  final double minWidth = 56;

  late Animation<double> hide;

  late Animation<double> hideOpacity;

  MaterialPropertyResolver<Color> get getRippleColor =>
      (Set<MaterialState> states) {
        return KaykeyColors.primaryVariant.withOpacity(0.2);
      };

  void _initAnimations() {
    hide = Tween<double>(begin: 0, end: 1).animate(CurvedAnimation(
        parent: widget.animation, curve: const Interval(0, 0.8)));
    hideOpacity = Tween<double>(begin: 0, end: 1).animate(CurvedAnimation(
        parent: widget.animation, curve: const Interval(0.8, 1)));
  }

  @override
  void initState() {
    _initAnimations();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: widget.animation,
      builder: (BuildContext context, Widget? child) {
        double width = hide.value.interpolate(
            from: minWidth, to: maxWidth, min: 0, max: 1); // hideAnimation

        double opacity = hideOpacity.value; // hideOpacity

        bool isHidden = opacity == 0;

        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Container(
            width: width,
            clipBehavior: Clip.antiAlias,
            decoration: ShapeDecoration(
              color: KaykeyColors.primary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
              ),
            ),
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: widget.onTap,
                overlayColor: MaterialStateColor.resolveWith(getRippleColor),
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      widget.item.icon,
                      isHidden
                          ? const SizedBox.shrink()
                          : const SizedBox(width: 8.0),
                      isHidden
                          ? const SizedBox.shrink()
                          : Opacity(
                              opacity: opacity,
                              child: Text(
                                widget.item.label,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(color: KaykeyColors.onPrimary),
                              ),
                            ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
      child: Container(),
    );
  }
}

class NavigationSideBar extends StatefulWidget {
  final List<NavigationSideBarItem> items;
  final int currentIndex;
  final bool allowExpansion;
  final ValueChanged<int>? onTap;

  final Color? backgroundColor;
  final IconThemeData? selectedIconTheme;
  final IconThemeData? unselectedIconTheme;
  final Color? selectedItemColor;
  final Color? unselectedItemColor;

  final double iconSize;

  final AnimationController controller;
  final VoidCallback onExpandNavigationBar;

  const NavigationSideBar(
      {Key? key,
      required this.controller,
      required this.onExpandNavigationBar,
      required this.items,
      this.currentIndex = 0,
      this.allowExpansion = true,
      this.onTap,
      this.iconSize = 24,
      this.selectedItemColor,
      this.unselectedItemColor,
      this.backgroundColor,
      this.selectedIconTheme,
      this.unselectedIconTheme})
      : super(key: key);

  @override
  _NavigationSideBarState createState() => _NavigationSideBarState();
}

class _NavigationSideBarState extends State<NavigationSideBar>
    with TickerProviderStateMixin {
  List<AnimationController> _controllers = <AnimationController>[];
  late List<CurvedAnimation> _animations;

  bool expand = true;

  void _resetState() {
    for (final AnimationController controller in _controllers) {
      controller.dispose();
    }

    _controllers =
        List<AnimationController>.generate(widget.items.length, (index) {
      return AnimationController(
        vsync: this,
        duration: kThemeAnimationDuration,
      )..addListener(_rebuild);
    });

    _animations = List.generate(widget.items.length, (index) {
      return CurvedAnimation(
        parent: _controllers[index],
        curve: Curves.fastOutSlowIn,
        reverseCurve: Curves.fastOutSlowIn.flipped,
      );
    });

    _controllers[widget.currentIndex].value = 1;
  }

  void _rebuild() {
    setState(() {});
  }

  @override
  void initState() {
    _resetState();
    super.initState();
  }

  @override
  void dispose() {
    for (final AnimationController controller in _controllers) {
      controller.dispose();
    }
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant NavigationSideBar oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget.items.length != oldWidget.items.length) {
      _resetState();
      return;
    }

    if (widget.currentIndex != oldWidget.currentIndex) {
      _controllers[oldWidget.currentIndex].reverse();
      _controllers[widget.currentIndex].forward();
    }
  }

  List<Widget> _createTiles() {
    final BottomNavigationBarThemeData bottomTheme =
        BottomNavigationBarTheme.of(context);

    ColorTween colorTween = ColorTween(
      begin: widget.unselectedItemColor ?? KaykeyColors.onPrimary,
      end: widget.selectedItemColor ?? KaykeyColors.onBackground,
    );

    List<Widget> tiles = [];
    for (int i = 0; i < widget.items.length; i++) {
      tiles.add(_NavigationSideBarTile(
        item: widget.items[i],
        selected: i == widget.currentIndex,
        animation: _animations[i],
        sizeAnimation: widget.controller,
        onTap: () {
          widget.onTap?.call(i);
        },
        colorTween: colorTween,
        iconSize: widget.iconSize,
        selectedIconTheme:
            widget.selectedIconTheme ?? bottomTheme.selectedIconTheme,
        unselectedIconTheme:
            widget.selectedIconTheme ?? bottomTheme.unselectedIconTheme,
      ));
    }
    return tiles;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _NavigationSideBarActionTile(
              item: NavigationSideBarItem(
                icon: AnimatedIcon(
                  icon: AnimatedIcons.menu_close,
                  progress: widget.controller,
                  color: KaykeyColors.onPrimary,
                ),
                label: AppLocalizations.of(context)!.menu,
              ),
              onTap: widget.onExpandNavigationBar,
              animation: widget.controller,
            ),
            ..._createTiles()
          ],
        ),
        Expanded(
          child: Container(),
        ),
      ],
    );
  }
}
