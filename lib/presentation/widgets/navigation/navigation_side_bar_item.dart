import 'package:flutter/material.dart';

class NavigationSideBarItem {
  final Widget icon;
  final Widget activeIcon;
  final String label;

  const NavigationSideBarItem(
      {Key? key, required this.icon, Widget? activeIcon, required this.label})
      : activeIcon = activeIcon ?? icon;
}
