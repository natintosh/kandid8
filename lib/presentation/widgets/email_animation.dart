import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rive/rive.dart';

class EmailAnimation extends StatefulWidget {
  final double size;

  const EmailAnimation({Key? key, required this.size}) : super(key: key);

  @override
  _EmailAnimationState createState() => _EmailAnimationState();
}

class _EmailAnimationState extends State<EmailAnimation> {
  final String assetName = 'assets/rive/email.riv';

  Artboard? artboard;

  SimpleAnimation? showEmail;

  void initializeRiveAnimation() async {
    try {
      final ByteData data = await rootBundle.load(assetName);

      final RiveFile riveFile = RiveFile.import(data);

      final Artboard? artboard = riveFile.artboardByName('Email Artboard');

      showEmail = SimpleAnimation('showEmail');

      if (artboard != null) {
        artboard.addController(showEmail!);
      }

      setState(() {
        this.artboard = artboard;
      });
    } on Exception catch (e) {
      log('$e');
    }
  }

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      initializeRiveAnimation();
    });
    super.initState();
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void dispose() {
    showEmail?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.square(
      dimension: widget.size,
      child: artboard == null
          ? Container()
          : Rive(
              artboard: artboard!,
            ),
    );
  }
}
