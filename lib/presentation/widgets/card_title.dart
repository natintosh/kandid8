import 'package:flutter/material.dart';

class CardTitle extends StatelessWidget {
  const CardTitle({
    Key? key,
    required this.icon,
    required this.title,
  }) : super(key: key);

  final Widget icon;
  final String title;

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Theme(
            data: themeData.copyWith(
                iconTheme: IconThemeData(color: themeData.primaryColor)),
            child: icon),
        const SizedBox(width: 16),
        Text(
          title,
          style: themeData.textTheme.subtitle2!.copyWith(
            color: themeData.primaryColor,
          ),
        ),
      ],
    );
  }
}
