import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'failure.freezed.dart';

@freezed
class Failure<T> with _$Failure<T> {
  factory Failure({required T error, String? message}) = _Failure<T>;
}
