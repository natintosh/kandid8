import 'package:dartz/dartz.dart';
import 'package:kaykey/domain/common/failure.dart';
import 'package:kaykey/domain/common/validators.dart';
import 'package:kaykey/domain/common/value_object.dart';

enum TextValueError {
  emptyText,
}

class TextValue extends ValueObject {
  final Either<Failure<TextValueError>, String> _value;
  @override
  Either<Failure<TextValueError>, String> get value => _value;

  const TextValue._(this._value);

  factory TextValue(String input) {
    return TextValue._(validateText(input));
  }
}
