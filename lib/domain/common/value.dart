import 'package:dartz/dartz.dart';
import 'package:kaykey/domain/common/failure.dart';
import 'package:kaykey/domain/common/validators.dart';
import 'package:kaykey/domain/common/value_object.dart';

enum EmailAddressValueError {
  invalidEmail,
}

class EmailAddressValue extends ValueObject {
  final Either<Failure<EmailAddressValueError>, String> _value;

  @override
  Either<Failure<EmailAddressValueError>, String> get value => _value;

  const EmailAddressValue._(this._value);

  factory EmailAddressValue(String input) {
    return EmailAddressValue._(validateEmailAddress(input));
  }
}

enum PasswordValueError {
  invalidPassword,
}

class PasswordValue extends ValueObject {
  final Either<Failure<PasswordValueError>, String> _value;

  @override
  Either<Failure<PasswordValueError>, String> get value => _value;

  const PasswordValue._(this._value);

  factory PasswordValue(String input) {
    return PasswordValue._(validatePassword(input));
  }
}
