import 'package:dartz/dartz.dart';
import 'package:kaykey/domain/common/failure.dart';
import 'package:kaykey/domain/common/text.value.dart';
import 'package:kaykey/domain/common/value.dart';

Either<Failure<TextValueError>, String> validateText(String value) {
  if (value.isNotEmpty) {
    return Right(value);
  }

  return Left(Failure(error: TextValueError.emptyText));
}

Either<Failure<EmailAddressValueError>, String> validateEmailAddress(
    String value) {
  const emailRegexp =
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*";

  if (RegExp(emailRegexp).hasMatch(value)) {
    return Right(value);
  }

  return Left(Failure(error: EmailAddressValueError.invalidEmail));
}

Either<Failure<PasswordValueError>, String> validatePassword(String value) {
  if (value.length > 6) {
    return Right(value);
  }

  return Left(Failure(error: PasswordValueError.invalidPassword));
}
