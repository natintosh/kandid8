// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'result.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ResultTearOff {
  const _$ResultTearOff();

  _OnSuccess<L, R> onSuccess<L, R>({required R value}) {
    return _OnSuccess<L, R>(
      value: value,
    );
  }

  _OnFailure<L, R> onFailure<L, R>({required L value}) {
    return _OnFailure<L, R>(
      value: value,
    );
  }
}

/// @nodoc
const $Result = _$ResultTearOff();

/// @nodoc
mixin _$Result<L, R> {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(R value) onSuccess,
    required TResult Function(L value) onFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(R value)? onSuccess,
    TResult Function(L value)? onFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(R value)? onSuccess,
    TResult Function(L value)? onFailure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_OnSuccess<L, R> value) onSuccess,
    required TResult Function(_OnFailure<L, R> value) onFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_OnSuccess<L, R> value)? onSuccess,
    TResult Function(_OnFailure<L, R> value)? onFailure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_OnSuccess<L, R> value)? onSuccess,
    TResult Function(_OnFailure<L, R> value)? onFailure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ResultCopyWith<L, R, $Res> {
  factory $ResultCopyWith(
          Result<L, R> value, $Res Function(Result<L, R>) then) =
      _$ResultCopyWithImpl<L, R, $Res>;
}

/// @nodoc
class _$ResultCopyWithImpl<L, R, $Res> implements $ResultCopyWith<L, R, $Res> {
  _$ResultCopyWithImpl(this._value, this._then);

  final Result<L, R> _value;
  // ignore: unused_field
  final $Res Function(Result<L, R>) _then;
}

/// @nodoc
abstract class _$OnSuccessCopyWith<L, R, $Res> {
  factory _$OnSuccessCopyWith(
          _OnSuccess<L, R> value, $Res Function(_OnSuccess<L, R>) then) =
      __$OnSuccessCopyWithImpl<L, R, $Res>;
  $Res call({R value});
}

/// @nodoc
class __$OnSuccessCopyWithImpl<L, R, $Res>
    extends _$ResultCopyWithImpl<L, R, $Res>
    implements _$OnSuccessCopyWith<L, R, $Res> {
  __$OnSuccessCopyWithImpl(
      _OnSuccess<L, R> _value, $Res Function(_OnSuccess<L, R>) _then)
      : super(_value, (v) => _then(v as _OnSuccess<L, R>));

  @override
  _OnSuccess<L, R> get _value => super._value as _OnSuccess<L, R>;

  @override
  $Res call({
    Object? value = freezed,
  }) {
    return _then(_OnSuccess<L, R>(
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as R,
    ));
  }
}

/// @nodoc

class _$_OnSuccess<L, R>
    with DiagnosticableTreeMixin
    implements _OnSuccess<L, R> {
  _$_OnSuccess({required this.value});

  @override
  final R value;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Result<$L, $R>.onSuccess(value: $value)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Result<$L, $R>.onSuccess'))
      ..add(DiagnosticsProperty('value', value));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _OnSuccess<L, R> &&
            const DeepCollectionEquality().equals(other.value, value));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(value));

  @JsonKey(ignore: true)
  @override
  _$OnSuccessCopyWith<L, R, _OnSuccess<L, R>> get copyWith =>
      __$OnSuccessCopyWithImpl<L, R, _OnSuccess<L, R>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(R value) onSuccess,
    required TResult Function(L value) onFailure,
  }) {
    return onSuccess(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(R value)? onSuccess,
    TResult Function(L value)? onFailure,
  }) {
    return onSuccess?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(R value)? onSuccess,
    TResult Function(L value)? onFailure,
    required TResult orElse(),
  }) {
    if (onSuccess != null) {
      return onSuccess(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_OnSuccess<L, R> value) onSuccess,
    required TResult Function(_OnFailure<L, R> value) onFailure,
  }) {
    return onSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_OnSuccess<L, R> value)? onSuccess,
    TResult Function(_OnFailure<L, R> value)? onFailure,
  }) {
    return onSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_OnSuccess<L, R> value)? onSuccess,
    TResult Function(_OnFailure<L, R> value)? onFailure,
    required TResult orElse(),
  }) {
    if (onSuccess != null) {
      return onSuccess(this);
    }
    return orElse();
  }
}

abstract class _OnSuccess<L, R> implements Result<L, R> {
  factory _OnSuccess({required R value}) = _$_OnSuccess<L, R>;

  R get value;
  @JsonKey(ignore: true)
  _$OnSuccessCopyWith<L, R, _OnSuccess<L, R>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$OnFailureCopyWith<L, R, $Res> {
  factory _$OnFailureCopyWith(
          _OnFailure<L, R> value, $Res Function(_OnFailure<L, R>) then) =
      __$OnFailureCopyWithImpl<L, R, $Res>;
  $Res call({L value});
}

/// @nodoc
class __$OnFailureCopyWithImpl<L, R, $Res>
    extends _$ResultCopyWithImpl<L, R, $Res>
    implements _$OnFailureCopyWith<L, R, $Res> {
  __$OnFailureCopyWithImpl(
      _OnFailure<L, R> _value, $Res Function(_OnFailure<L, R>) _then)
      : super(_value, (v) => _then(v as _OnFailure<L, R>));

  @override
  _OnFailure<L, R> get _value => super._value as _OnFailure<L, R>;

  @override
  $Res call({
    Object? value = freezed,
  }) {
    return _then(_OnFailure<L, R>(
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as L,
    ));
  }
}

/// @nodoc

class _$_OnFailure<L, R>
    with DiagnosticableTreeMixin
    implements _OnFailure<L, R> {
  _$_OnFailure({required this.value});

  @override
  final L value;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Result<$L, $R>.onFailure(value: $value)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Result<$L, $R>.onFailure'))
      ..add(DiagnosticsProperty('value', value));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _OnFailure<L, R> &&
            const DeepCollectionEquality().equals(other.value, value));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(value));

  @JsonKey(ignore: true)
  @override
  _$OnFailureCopyWith<L, R, _OnFailure<L, R>> get copyWith =>
      __$OnFailureCopyWithImpl<L, R, _OnFailure<L, R>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(R value) onSuccess,
    required TResult Function(L value) onFailure,
  }) {
    return onFailure(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(R value)? onSuccess,
    TResult Function(L value)? onFailure,
  }) {
    return onFailure?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(R value)? onSuccess,
    TResult Function(L value)? onFailure,
    required TResult orElse(),
  }) {
    if (onFailure != null) {
      return onFailure(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_OnSuccess<L, R> value) onSuccess,
    required TResult Function(_OnFailure<L, R> value) onFailure,
  }) {
    return onFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_OnSuccess<L, R> value)? onSuccess,
    TResult Function(_OnFailure<L, R> value)? onFailure,
  }) {
    return onFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_OnSuccess<L, R> value)? onSuccess,
    TResult Function(_OnFailure<L, R> value)? onFailure,
    required TResult orElse(),
  }) {
    if (onFailure != null) {
      return onFailure(this);
    }
    return orElse();
  }
}

abstract class _OnFailure<L, R> implements Result<L, R> {
  factory _OnFailure({required L value}) = _$_OnFailure<L, R>;

  L get value;
  @JsonKey(ignore: true)
  _$OnFailureCopyWith<L, R, _OnFailure<L, R>> get copyWith =>
      throw _privateConstructorUsedError;
}
