import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'result.freezed.dart';

@freezed
class Result<L, R> with _$Result<L, R> {
  factory Result.onSuccess({required R value}) = _OnSuccess<L, R>;

  factory Result.onFailure({required L value}) = _OnFailure<L, R>;
}
