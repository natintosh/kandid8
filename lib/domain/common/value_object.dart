import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:kaykey/domain/common/failure.dart';

@immutable
abstract class ValueObject<T> {
  const ValueObject();
  Either<Failure<T>, T> get value;

  Either<Failure<dynamic>, Unit> get failureOrUnit {
    return value.fold(
      (l) => left(l),
      (r) => right(unit),
    );
  }

  bool isValid() => value.isRight();

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ValueObject<T> && other.value == value;
  }

  @override
  int get hashCode => value.hashCode;

  @override
  String toString() => 'Value($value)';
}
