import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:kaykey/domain/common/failure.dart';
import 'package:kaykey/domain/common/result.dart';
import 'package:kaykey/domain/common/text.value.dart';
import 'package:kaykey/domain/common/value.dart';

enum CreateUserErrorCode {
  emailAlreadyInUse,
  internalError,
  invalidEmail,
  weakPassword,
  internal,
}

enum SignInErrorCode {
  invalidEmail,
  userDisabled,
  userNotFound,
  wrongPassword,
  internal,
}

enum EmailVerificationErrorCode {
  missingAndroidPackageName,
  missingContinueUri,
  missingiOSBundleId,
  invalidContinueUri,
  unauthorizeContinueUri,
  internalError,
  internal,
}

enum ActionCodeErrorCode {
  expiredActionCode,
  invalidActionCode,
  userDisabled,
  userNotFound,
  internal,
}

abstract class AuthenticationFacade {
  Future<Result<Failure<CreateUserErrorCode>, UserCredential>>
      createUserWithEmailAndPassword(
          {required TextValue displayName,
          required EmailAddressValue emailAddress,
          required PasswordValue password});

  Future<Result<Failure<SignInErrorCode>, UserCredential>>
      signInWithEmailAndPassword({
    required EmailAddressValue emailAddress,
    required PasswordValue password,
  });

  Future<Result<Failure<SignInErrorCode>, UserCredential>> signInWithGoogle();

  Future<Result<Failure<EmailVerificationErrorCode>, Unit>>
      sendEmailVerification({required ActionCodeSettings? settings});

  Future<Result<Unit, Unit>> isUserVerified();

  Future<Result<Unit, User?>> getFirebaseUser();

  Future<Result<Failure<ActionCodeErrorCode>, ActionCodeInfo>> checkActionCode(
      {required TextValue actionCode});

  Future<Result<Failure<ActionCodeErrorCode>, Unit>> applyActionCoode(
      {required TextValue actionCode});
}
